#! /bin/bash

. ../parm.sh
STEP=calc_srh.sh

#memo
#undef msm=9.999e20, msm_topo=-999.9, grads(def) -9.99e8
undef=9.999e20

fort/srh.exe <<EOF 
$FNUM3h
241,253,21
$undef
'$MSMBIN'
'UGRDz.bin','VGRDz.bin'
'SRH.bin','SRHPO.bin'
EOF
#fi

tdefl=`cat $MSMBIN/HGTprs.ctl | grep tdef`
tgrads=`echo $tdefl | cut -d " " -f 4`
it=`echo $tdefl | cut -d " " -f 2`

cat << EOF > $MSMBIN/SRH.ctl
dset ^SRH.bin
undef 9.999E+20
title srotm relative helicity
options big_endian
xdef 241 linear 120.000000 0.125000
ydef 253 linear 22.400000 0.1000
tdef $it linear $tgrads 3hr
zdef 1 levels 1
vars 7
srh3 1 0 ** srh from surface to 3000m AGL
srh1 1 0 ** srh from surface to 1000m AGL
uc 1 0 ** srtom motion (Bunkers etal., 2000)
vc 1 0 **
um 1 0 ** mean wind from surface to 6000m AGL
vm 1 0 **
sfc 1 0 ** lowest level number
ENDVARS
EOF

exit

