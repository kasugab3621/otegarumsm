#!/bin/bash
## make MSM ctl & idx file
## 2014/12/08 Corded by Yusuke KIMURA

PARMF="../parm.sh"
. $PARMF

STEP=make_msm.ctl
#set +evx
## install check
CHg2ctl=`which g2ctl`
if [ ${#CHg2ctl} -eq '0' -a ! -f g2ctl ]; then
    echo "Error @ mkctl_msm.sh\n    ===> g2ctl does not exist."
    exit
fi

for fl in $MSMORG/*.bin; do
#    echo $fl
    STLG=${#fl}
    K1=`expr $STLG - 30`
    K2=`expr $STLG - 27`
    KIND=`echo $fl | cut -c ${K1}-${K2}`
    D1=`expr $STLG - 17`
    D2=`expr $STLG - 8`
    DATE=`echo $fl | cut -c ${D1}-${D2}`
    ctlname=view_MSM_${KIND}_${DATE}.ctl
#    echo $ctlname
    g2ctl $fl > $CTLDIR/$ctlname
    gribmap -i $CTLDIR/$ctlname
done

