#!/bin/sh
set -exv

CRTDIR=`pwd`
wget ftp://ftp.cpc.ncep.noaa.gov/wd51we/wgrib2/wgrib2.tgz
tar zxvf wgrib2.tgz
cd grib2/
make
sleep 1
cd $CRTDIR

rm -rf wgrib2.tgz

set +exv
echo "Please cp wgrib2 to bin-dir."
echo "==> $ sudo cp ~/Download/grib2/wgrib2/wgrib2 /usr/local/bin/."


