#! /bin/bash

. ../parm.sh

STEP=cutout-binarry

mkdir -p ${MSMBIN}

if [ ${YYS}${MMS}${DDS} -lt 20170331 ] ;then
    pfile=var_pall.txt
else
    pfile=var_pall-vv.txt
fi

sfile=var_surf.txt

cat ${SHDIR}/${pfile} | while read vars ;do
ttt=1

for cf in $CTLDIR/*pall*.ctl ;do

case $ttt in
? ) tn=00$ttt ;;
?? ) tn=0$ttt ;;
??? ) tn=$ttt ;;
esac

if [ $ttt = 1 ];then
tdefl=`cat $cf | grep tdef`
tgrads=`echo $tdefl | cut -d " " -f 4`
fi
fn=${tn}_${vars}.bin

echo "making $fn"
grads -blc "scut_mpall.gs $cf $vars $fn" 1> /dev/null 2> /dev/null
#echo $cf $vars $fn
ttt=`expr $ttt + 1`
done  ;# pall loop (ttt)

echo "combining $vars.bin"
cat $SHDIR/???_$vars.bin > $MSMBIN/$vars.bin

tte=`expr $ttt - 1`

zz=16
levs="1000 975 950 925 900 850 800 700 600 500 400 300 250 200 150 100"
cat << EOF > $MSMBIN/$vars.ctl
dset ^$vars.bin
undef 9.999E+20
title $vars
options big_endian
xdef 241 linear 120.000000 0.125000
ydef 253 linear 22.400000 0.1000
tdef $tte linear $tgrads 3hr
zdef $zz levels $levs
vars 1
$vars $zz 0 **
ENDVARS
EOF

done

cat ${SHDIR}/${sfile} | while read varss ;do

ttt=1
for ccf in $CTLDIR/*surf*.ctl ;do
case $ttt in
? ) tn=00$ttt ;;
?? ) tn=0$ttt ;;
??? ) tn=$ttt ;;
esac
fnn=${tn}_${varss}.bin

if [ $ttt = 1 ];then
tdefl=`cat $ccf | grep tdef`
tgrads=`echo $tdefl | cut -d " " -f 4`
fi

echo "making $fnn"
grads -blc "scut_msurf.gs $ccf $varss $fnn" 1> /dev/null 2> /dev/null

ttt=`expr $ttt + 1`
done  ;# surf loop

echo "combining $varss.bin"
cat $SHDIR/???_$varss.bin > $MSMBIN/$varss.bin

tte=`expr $ttt - 1`

cat << EOF > $MSMBIN/$varss.ctl
dset ^$varss.bin
undef 9.999E+20
title $varss
options big_endian
xdef 481 linear 120.000000 0.062500
ydef 505 linear 22.400000 0.05
tdef $tte linear $tgrads 3hr
zdef 1 linear 1 1
vars 1
$varss 0 0 **
ENDVARS
EOF
done

rm *_*.bin

exit
