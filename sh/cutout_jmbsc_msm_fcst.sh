#! /bin/bash

. ../parm.sh

STEP=cutout-binarry

mkdir -p ${MSMBIN}

# jump pall to surf section
if [ 1 = 1 ] ; then

cat ${SHDIR}/var_pall.txt | while read vars ;do

for cf in $CTLDIR/*pall*.ctl ;do

if [ `echo $cf | grep 'FH'` ]; then
    :
else
    continue
fi

fh=`echo $cf | rev | cut -d"/" -f1 | rev | cut -d"_" -f5 | cut -d"." -f1`
dt=`echo $cf | rev | cut -d"/" -f1 | rev | cut -d"_" -f4`

if [ $fh = "FH00-15" ] ; then
tdefl=`cat $cf | grep tdef`
tgrads=`echo $tdefl | cut -d " " -f 4`
fi

if [ $fh = "FH36-39" ] ; then
    et=2
else
    et=6
fi

fn=${dt}_${fh}_${vars}.bin
echo "making $fn"
grads -blc "scut_mpall_fcst.gs $cf $vars $fn $et" 1> /dev/null 2> /dev/null
#echo $cf $vars $fn

if [ $fh = "FH36-39" ] ; then

echo "combining ${dt}_$vars.bin"
cat $SHDIR/${dt}_FH??-??_$vars.bin > $MSMBIN/${dt}_$vars.bin
rm -rf $SHDIR/${dt}_FH??-??_$vars.bin

zz=16
levs="1000 975 950 925 900 850 800 700 600 500 400 300 250 200 150 100"

cat << EOF > $MSMBIN/${dt}_$vars.ctl
dset ^${dt}_$vars.bin
undef 9.999E+20
title $vars
options big_endian
xdef 241 linear 120.000000 0.125000
ydef 253 linear 22.400000 0.1000
tdef 14 linear $tgrads 3hr
zdef $zz levels $levs
vars 1
$vars $zz 0 **
ENDVARS
EOF

fi

done ;#time loop

done ;#vars loop

##################################

fi ;#jump

cat ${SHDIR}/var_surf.txt | while read vars ;do

for cf in $CTLDIR/*surf*.ctl ;do

if [ `echo $cf | grep 'FH'` ]; then
    :
else
    continue
fi
    
fh=`echo $cf | rev | cut -d"/" -f1 | rev | cut -d"_" -f5 | cut -d"." -f1`
dt=`echo $cf | rev | cut -d"/" -f1 | rev | cut -d"_" -f4`

if [ $fh = "FH00-15" ] ; then
tdefl=`cat $cf | grep tdef`
tgrads=`echo $tdefl | cut -d " " -f 4`
fi

if [ $fh = "FH34-39" ] ; then
    et=6
else
    et=16
fi

fn=${dt}_${fh}_${vars}.bin
echo "making $fn"
grads -blc "scut_msurf_fcst.gs $cf $vars $fn $et" 1> /dev/null 2> /dev/null
#echo $cf $vars $fn

if [ $fh = "FH34-39" ] ; then

echo "combining ${dt}_$vars.bin"
cat $SHDIR/${dt}_FH??-??_$vars.bin > $MSMBIN/${dt}_$vars.bin
rm -rf $SHDIR/${dt}_FH??-??_$vars.bin

cat << EOF > $MSMBIN/${dt}_$vars.ctl
dset ^${dt}_$vars.bin
undef 9.999E+20
title $vars
options big_endian
xdef 481 linear 120.000000 0.062500
ydef 505 linear 22.400000 0.05
tdef 38 linear $tgrads 1hr
zdef 1 linear 1 1
vars 1
$vars 0 0 **
ENDVARS
EOF

fi

done ;#time loop

done ;#vars loop


#rm *FH*.bin

exit

