dset ^tmp_link/Z__C_RJTD_20171231151000_RDR_JMAGPV_Ggis1km_Prr10lv_ANAL_grib2.bin
index ^tmp_link/Z__C_RJTD_20171231151000_RDR_JMAGPV_Ggis1km_Prr10lv_ANAL_grib2.bin.idx
undef 9.999E+20
title tmp_link/Z__C_RJTD_20171231151000_RDR_JMAGPV_Ggis1km_Prr10lv_ANAL_grib2.bin
* produced by g2ctl v0.1.1
* command line options: tmp_link/Z__C_RJTD_20171231151000_RDR_JMAGPV_Ggis1km_Prr10lv_ANAL_grib2.bin
* griddef=1:0:(2560 x 3360):grid_template=0:winds(N/S): lat-lon grid:(2560 x 3360) units 1e-06 input WE:NS output WE:SN res 48 lat 47.995833 to 20.004167 by 0.008333 lon 118.006250 to 149.993750 by 0.012500 #points=8601600:winds(N/S)

dtype grib2
ydef 3360 linear 20.004167 0.008333
xdef 2560 linear 118.006250 0.012500
tdef 1 linear 15:10Z31dec2017 1mo
zdef 1 linear 1 1
vars 1
var01201sfc  0,1   0,1,201 ** surface desc [unit]
ENDVARS
