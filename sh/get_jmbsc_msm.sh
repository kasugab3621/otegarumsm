#!/bin/bash
## get MSM data
## ( <- JMA-NHM ~/Nhm/Tools/grb2nus/get_jmbsc_msm.sh)
## 2014/12/08 Edited by Yusuke KIMURA

PARMF="../parm.sh"
. $PARMF

# recieve UTC from parm.sh
yr=$YYS
mo=$MMS
dy=$DDS
hr=$HHS
mn=$NNS
end_date=$DATEED

#echo $yr/$mo/$dy $hr:$mn

## in case mn != 00,go next time
if [ $mn -ne 00 ]; then
    dfmn=`expr 60 - $mn`
    date=`$d_cmd -d "$yr/$mo/$dy $hr:$mn $dfmn minutes" +%Y%m%d%H%M`
    yr=`echo $date | cut -c 1-4`
    mo=`echo $date | cut -c 5-6`
    dy=`echo $date | cut -c 7-8`
    hr=`echo $date | cut -c 9-10`
    mn=`echo $date | cut -c 11-12`
fi

#echo $yr/$mo/$dy $hr:$mn

## in case hr != 0 (mod 3), adjust hr = 0 (mod 3)
if [ $hr -ne 00 -a $hr -ne 03 -a $hr -ne 06 -a $hr -ne 09 -a $hr -ne 12 -a $hr -ne 15 -a $hr -ne 18 -a $hr -ne 21 ]; then
    rmnd=`expr $hr \% 3`
    dfhr=`expr 3 - $rmnd`
    date=`$d_cmd -d "$yr/$mo/$dy $hr:$mn $dfhr hours" +%Y%m%d%H%M`
    yr=`echo $date | cut -c 1-4`
    mo=`echo $date | cut -c 5-6`
    dy=`echo $date | cut -c 7-8`
    hr=`echo $date | cut -c 9-10`
    mn=`echo $date | cut -c 11-12`
fi

#echo $yr/$mo/$dy $hr:$mn
date=$yr$mo$dy$hr$mn

YYYY=$yr
MO=$mo
DD=$dy
HH=$hr
MI=00
ihr=0

while [ $date -le $end_date ]; do
    echo $YY/$MO/$DD $HH:$MI
    
    USE_PROXY="off" # "on" or "off" 
    
    if [ ${YYYY}${MO}${DD} -lt "20060301" ] ;then
	echo "Not supported before 2006/03/01"
	exit 99
    fi
    
    if [ ${USE_PROXY} = "on" ] ; then
	http_proxy="http://10.3.1.11:8080/" # Change for your proxy_server
	export http_proxy
    fi
    
    DATA_SERVER=database.rish.kyoto-u.ac.jp
    DATA_PATH=arch/jmadata/data/gpv/original
    if [ ${YYYY}${MO}${DD}${HH}${MI} -lt "200205220000" ] ; then
	echo "You can select after \"2002-May-22 00UTC\" in JMBSC data"
	exit
    fi
    
#    DATADIR=`cd ../../; pwd`/Data/jmbsc_msm #Nhm/Data/jmbsc
#    SHDIR=`pwd`
    
#    SAVEDIR=${DATADIR}/${YYYY}${MO}${DD}${HH}${MI}
    SAVEDIR=${MSMORG}/
    if [ ! -d ${SAVEDIR} ] ;then
	mkdir -p ${SAVEDIR}
    fi
    
    cd ${SAVEDIR}
    
    echo "download Z__C_RJTD_${YYYY}${MO}${DD}${HH}${MI}00_MSM_GPV_Rjp_L-pall_FH00-15_grib2.bin"
    wget --dot-style=mega -c -T 20 -t 30 --proxy=${USE_PROXY} http://${DATA_SERVER}/${DATA_PATH}/${YYYY}/${MO}/${DD}/Z__C_RJTD_${YYYY}${MO}${DD}${HH}${MI}00_MSM_GPV_Rjp_L-pall_FH00-15_grib2.bin
    mv Z__C_RJTD_${YYYY}${MO}${DD}${HH}${MI}00_MSM_GPV_Rjp_L-pall_FH00-15_grib2.bin MSM_GPV_pall_FH00-15_${YYYY}${MO}${DD}${HH}${MI}00.bin
    echo "download Z__C_RJTD_${YYYY}${MO}${DD}${HH}${MI}00_MSM_GPV_Rjp_Lsurf_FH00-15_grib2.bin"
    wget --dot-style=mega -c -T 20 -t 30 --proxy=${USE_PROXY} http://${DATA_SERVER}/${DATA_PATH}/${YYYY}/${MO}/${DD}/Z__C_RJTD_${YYYY}${MO}${DD}${HH}${MI}00_MSM_GPV_Rjp_Lsurf_FH00-15_grib2.bin
    mv Z__C_RJTD_${YYYY}${MO}${DD}${HH}${MI}00_MSM_GPV_Rjp_Lsurf_FH00-15_grib2.bin MSM_GPV_surf_FH00-15_${YYYY}${MO}${DD}${HH}${MI}00.bin
    

    ihr=`expr $ihr + 3`
    date=`$d_cmd -d "$yr/$mo/$dy $hr:00 $ihr hours" +%Y%m%d%H%M`
    YYYY=`echo $date | cut -c 1-4`
    MO=`echo $date | cut -c 5-6`
    DD=`echo $date | cut -c 7-8`
    HH=`echo $date | cut -c 9-10`
    MI=00
done


exit



##--- ORIGINAL CORD---
#read YYYY MO DD HH MI < TIMECARD
#
#USE_PROXY="off" # "on" or "off" 
#
#if [ ${YYYY}${MO}${DD} -lt "20060301" ] ;then
#	echo "Not supported before 2006/03/01"
#	exit 99
#fi
#
#if [ ${USE_PROXY} = "on" ] ; then
#    http_proxy="http://10.3.1.11:8080/" # Change for your proxy_server
#    export http_proxy
#fi
#
#DATA_SERVER=database.rish.kyoto-u.ac.jp
#DATA_PATH=arch/jmadata/data/gpv/original
#if [ ${YYYY}${MO}${DD}${HH}${MI} -lt "200205220000" ] ; then
#  echo "You can select after \"2002-May-22 00UTC\" in JMBSC data"
#  exit
#fi
#
#DATADIR=`cd ../../; pwd`/Data/jmbsc_msm #Nhm/Data/jmbsc
#SHDIR=`pwd`
#
#SAVEDIR=${DATADIR}/${YYYY}${MO}${DD}${HH}${MI}
#if [ ! -d ${SAVEDIR} ] ;then
#    mkdir -p ${SAVEDIR}
#fi
#
#cd ${SAVEDIR}
#
#echo "download Z__C_RJTD_${YYYY}${MO}${DD}${HH}${MI}00_MSM_GPV_Rjp_L-pall_FH00-15_grib2.bin"
#wget --dot-style=mega -c -T 20 -t 30 --proxy=${USE_PROXY} http://${DATA_SERVER}/${DATA_PATH}/${YYYY}/${MO}/${DD}/Z__C_RJTD_${YYYY}${MO}${DD}${HH}${MI}00_MSM_GPV_Rjp_L-pall_FH00-15_grib2.bin
#echo "download Z__C_RJTD_${YYYY}${MO}${DD}${HH}${MI}00_MSM_GPV_Rjp_Lsurf_FH00-15_grib2.bin"
#wget --dot-style=mega -c -T 20 -t 30 --proxy=${USE_PROXY} http://${DATA_SERVER}/${DATA_PATH}/${YYYY}/${MO}/${DD}/Z__C_RJTD_${YYYY}${MO}${DD}${HH}${MI}00_MSM_GPV_Rjp_Lsurf_FH00-15_grib2.bin
#
#case ${HH} in
#    03|09|15|21)
#    if [ ${YYYY}${MO}${DD} -ge "20070516" ] ;then
#	echo "download Z__C_RJTD_${YYYY}${MO}${DD}${HH}${MI}00_MSM_GPV_Rjp_L-pall_FH18-33_grib2.bin"
#        wget --dot-style=mega -c -T 20 -t 30 --proxy=${USE_PROXY} http://${DATA_SERVER}/${DATA_PATH}/${YYYY}/${MO}/${DD}/Z__C_RJTD_${YYYY}${MO}${DD}${HH}${MI}00_MSM_GPV_Rjp_L-pall_FH18-33_grib2.bin
#	echo "download Z__C_RJTD_${YYYY}${MO}${DD}${HH}${MI}00_MSM_GPV_Rjp_Lsurf_FH16-33_grib2.bin"
#        wget --dot-style=mega -c -T 20 -t 30 --proxy=${USE_PROXY} http://${DATA_SERVER}/${DATA_PATH}/${YYYY}/${MO}/${DD}/Z__C_RJTD_${YYYY}${MO}${DD}${HH}${MI}00_MSM_GPV_Rjp_Lsurf_FH16-33_grib2.bin
#    fi
#    ;;
#esac
