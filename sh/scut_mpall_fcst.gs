*---------------------------------------------------------------------
* cutout data from JRA-55 to GrADS
* 2014.01.08 corded by Yusuke KIMURA
*         ?? added z, y, axis loop
* 2015.05.26 Japanese -> English by kasuga
*---------------------------------------------------------------------

function cutout( args )

CTLF  = subwrd( args,1 )
VARS  = subwrd( args,2 )
FLNM  = subwrd( args,3 )
ENDT  = subwrd( args,4 )

XLEVS = "1 241"
YLEVS = "1 253"
EDIA  = "be"

'reinit'

* Main work
*---------------------------------------------------------------------

'open 'CTLF
'set fwrite -'EDIA' 'FLNM
'set undef dfile'
'set gxout fwrite'
'set x 'XLEVS
'set y 'YLEVS
'set z 1'
t=1;while(t<=ENDT)
'set t 't

   IZ=1
   while(IZ<=16)
      'd 'VARS'(z='IZ')'
      IZ=IZ+1
   endwhile

t=t+1;endwhile
'disable fwrite'
'quit'


return
*---------------------------------------------------------------------

