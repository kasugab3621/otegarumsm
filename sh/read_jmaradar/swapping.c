#include "grib2.h"

int swapping( void *p, enum integer_type itype )
{
  union{
    char c[4];
    unsigned short us;
    unsigned int ui;
  } u;
  
  char minus;
  
  minus = *( (char *)p ) & 0x80;
  
  if( need_byte_swapping ){
    if( itype == it_short ){
      u.c[0] = *( (char *)p + 1 );
      u.c[1] = *( (char *)p + 0 );
    }
    if( itype == it_int ){
      u.c[0] = *( (char *)p + 3 );
      u.c[1] = *( (char *)p + 2 );
      u.c[2] = *( (char *)p + 1 );
      u.c[3] = *( (char *)p + 0 );
    }
  }
  else{
    if( itype == it_short ){
      u.c[0] = *( (char *)p + 0 );
      u.c[1] = *( (char *)p + 1 );
    }
    if( itype == it_int ){
      u.c[0] = *( (char *)p + 0 );
      u.c[1] = *( (char *)p + 1 );
      u.c[2] = *( (char *)p + 2 );
      u.c[3] = *( (char *)p + 3 );
    }
  }
  
  if( minus ){
    if( itype == it_short )
      return( -1 * (u.us & 0x7fff) );
    else
      return( -1 * (u.ui &0x7fffffff) );
  }
  else{
    if( itype == it_short )
      return( u.us );
    else
      return( u.ui );
  }
  
}
  
