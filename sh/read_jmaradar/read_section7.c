#include <stdio.h>
#include <stdlib.h>
#include "grib2.h"
#include "math.h"

int read_section7( FILE *fp, struct grib2 *g )
{
  
  unsigned char buf[5];
  int length;
  unsigned char *data;
  int cur_pos;
  int n,num;
  int d1,n1;
  int ij;
  int nn;
  int m;
  float aa;
  int total_num;

  if( fread( buf, 1, 5, fp ) != 5 )
    return(0);
  
  if( buf[4] != 7 ) return(0);
  
  length = swapping( buf, it_int );
  length = length -5;

  /*length must be plus one 
    because d1 and n1 read cur_pos and cur_pos+1,respectively */
  data = (unsigned char *)malloc(sizeof(unsigned char)*(length+1));
  fread(data,length,sizeof(unsigned char),fp);

  //  printf("data length = %d \n",length);

  cur_pos = 0;
  ij      = 0;

  aa = pow(2,g->s5.Vbit) -1 - g->s5.VMAX;

  /*  printf("aa = %d \n", (int)(aa));
      printf("Vbit = %d \n", g->s5.Vbit);
      printf("VMAX = %d \n", g->s5.VMAX);*/

  while(cur_pos < length){
    
    d1 = (int)(data[cur_pos]);
    n1 = (int)(data[cur_pos+1]);

    n    = 0;
    if(n1 > g->s5.VMAX){
      check_next(cur_pos+1,&n,data,length,g->s5.VMAX);    
      total_num = 0;
      for(nn=0;nn<=n;nn++){
	total_num = total_num +
	  (int)((data[cur_pos+1+nn]- g->s5.VMAX -1)*pow(aa,nn));
      }
      total_num = total_num + 1;
      for(m=0;m<total_num;m++){
	if(d1 == 0){
	  g->rrain[ij+m] = -999.9;
	}else{
	  g->rrain[ij+m] = g->s5.RR[d1];
	}
      }
      ij = ij +total_num;
      //      printf("cur_pos1= %d d1=%d ij=%d n=%d\n",cur_pos,d1,ij,n+2);
      cur_pos = cur_pos + n+2;
    }else{

      if(d1 == 0){
	g->rrain[ij] = -999.9;
      }else{
      	g->rrain[ij] = g->s5.RR[d1];
      }
      ij = ij + 1;
      //printf("cur_pos2= %d d1=%d ij=%d n=1\n",cur_pos,d1,ij);
      cur_pos = cur_pos + 1;
    }

  }

  if(ij != g->s5.datanum){
    printf("data read is failed \n");
    printf("ij= %d datanum =%d \n",ij,g->s5.datanum);
    exit(1);
  }else{
    printf("data read is succeeded \n");
  }

  //  printf("ij = %d \n",ij);


  
  return(1);

}
