#include <stdio.h>
#include <stdlib.h>
#include "grib2.h"
#include "math.h"
#include <string.h>
#include <stdlib.h>

int read_section3( FILE *fp, struct grib2_section3 *s3 )
{
  
  unsigned char buf[72];
  double xe, ye;
  int i;
  
  if( fread( buf, 1, 72, fp ) != 72 ){
    return(0);
  }

  if( buf[4] != 3 ){ /* section number */
    return(0);
  }

  if( buf[5] != 0 ) /* Grid Definition: Lat/Lon */
    return(0);      /* see Code Tables 3.0 and 3.1 */
  
  s3->nx = swapping( buf+30, it_int );
  s3->ny = swapping( buf+34, it_int );
  
      ye = swapping( buf+46, it_int ) * 1.0e-6;
  s3->x0 = swapping( buf+50, it_int ) * 1.0e-6;

  s3->y0 = swapping( buf+55, it_int ) * 1.0e-6;
      xe = swapping( buf+59, it_int ) * 1.0e-6;
  
  s3->dx = swapping( buf+63, it_int ) * 1.0e-6;
  s3->dy = swapping( buf+67, it_int ) * 1.0e-6;
  s3->dy = 0.008333333;

  s3->lat = (void*)malloc(sizeof(double)*s3->ny);
  s3->lon = (void*)malloc(sizeof(double)*s3->nx);

  for(i=0;i<s3->nx;i++){
    s3->lon[i] = s3->x0 + (double)(i)*s3->dx;
  }

  for(i=0;i<s3->ny;i++){
    s3->lat[i] = s3->y0 + (double)(i)*s3->dy;
  }

  printf("nx = %d ny = %d \n",s3->nx,s3->ny);
  printf("dx = %f degree   dy = %f degree\n",s3->dx,s3->dy);
  printf("x0 = %f degree   y0 = %f degree\n",s3->x0,s3->y0);
  printf("xe = %f degree   ye = %f degree\n",xe,ye);
  printf("xe = %f degree   ye = %f degree\n",s3->lon[s3->nx-1],
	 s3->lat[s3->ny-1]);

  
  return(1);

}
