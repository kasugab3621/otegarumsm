#include <stdio.h>

#define MSM_3D 1
#define MSM_2D 2

unsigned char need_byte_swapping;

struct grib2_section1{
  int iyear, imon, iday, ihour, imin, isec;
};

struct grib2_section3{
  int nx, ny;
  double  x0, y0, dx, dy;
  double *lon;
  double *lat; 
};

struct grib2_section5{
  int VMAX;
  int level;
  int Vbit;
  float *RR;
  int   datanum;
};
  

struct grib2{
  struct grib2_section1 s1;
  struct grib2_section3 s3;  
  struct grib2_section5 s5;  
  float *rrain;
};
  
int read_section0( FILE *fp );
int read_section1( FILE *fp, struct grib2_section1 *s1 );
int read_section3( FILE *fp, struct grib2_section3 *s3 );
int read_section4( FILE *fp, struct grib2 *g );
int read_section5( FILE *fp, struct grib2_section5 *s5 );
int read_section6( FILE *fp );
int read_section7( FILE *fp, struct grib2 *g );
void data_write(FILE *fo, struct grib2 *g);

void check_next(int cur_pos,int *n, 
		unsigned char *data, int length, short VMAX);

enum integer_type{ it_short, it_int };
int swapping( void *p, enum integer_type itype );
float swapping_float( void *p );

extern unsigned char need_byte_swapping;  /* see swapping.c */

