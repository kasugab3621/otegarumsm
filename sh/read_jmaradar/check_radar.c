#include <stdio.h>
#include <stdlib.h>
#include "grib2.h"

int check_radar( FILE *fp){
  
  unsigned char buf[82];
  int length;
  unsigned char pcat, pnum;
  unsigned char ltype;
  int prs;
  
  if( fread( buf, 1, 4, fp ) != 4 )
    return(0);
  
  if( buf[0] == '7' && buf[1] == '7' && buf[2] == '7' && buf[3] == '7' ){
    return(0);
  }
  
  length = swapping( buf, it_int );
  if( length != 82 ){
    fprintf( stderr, "Error at section 4. It may not be JMA-RADAR.\n" );
    exit(1);
  }
  
  if( fread( buf+4, 1, length-4, fp ) != length-4 )
    return(0);
  
  if( buf[4] != 4 ) /* section number */
    return(0);
  
  fseek(fp,-length,SEEK_CUR);

  return 1;
  
  
}
