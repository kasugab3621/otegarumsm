#!/bin/bash

c_compiler=gcc-10

$c_compiler auto_endian_check.c -o auto_endian_check.o
$c_compiler check_next.c -o check_next.o
$c_compiler check_radar.c -o check_radar.o
$c_compiler data_write.c -o data_write.o
$c_compiler main.c -o main.o
$c_compiler read_section0.c -o read_section0.o
$c_compiler read_section1.c -o read_section1.o
$c_compiler read_section3.c -o read_section3.o
$c_compiler read_section4.c -o read_section4.o
$c_compiler read_section5.c -o read_section5.o
$c_compiler read_section6.c -o read_section6.o
$c_compiler read_section7.c -o read_section7.o
$c_compiler swapping.c -o swapping.o
$c_compiler swapping_float.c -o swapping_float.o

