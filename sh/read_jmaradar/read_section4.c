#include <stdio.h>
#include "grib2.h"
#include <string.h>
#include <stdlib.h>

int read_section4( FILE *fp, struct grib2 *g )
{
  
  unsigned char buf[82];
  int length;
  unsigned char pcat, pnum;
  unsigned char ltype;
  int test;

  if( fread( buf, 1, 4, fp ) != 4 )
    return(0);
  
  test = swapping(buf,it_int);

  if( buf[0] == '7' && buf[1] == '7' && buf[2] == '7' && buf[3] == '7' ){
    return(0);
  }
  
  
  length = swapping( buf, it_int );
  if( length != 82 ){
    fprintf( stderr, "Error at section 4. It may not be JMA-RADAR.\n" );
    exit(1);
  }
  
  if( fread( buf+4, 1, length-4, fp ) != length-4 ) return(0);
  
  return(1);

}
