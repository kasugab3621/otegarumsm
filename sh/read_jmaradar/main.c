#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "grib2.h"

float ERROR_UNDEF= 0.0;

int main(int argc, char **argv){

  char infile[100];
  char outfile[100];
  FILE *fp;
  FILE *fo;
  int status;
  struct grib2 g;
  float slon;
  float slat;
  float elon;
  float elat;

  int nxx,nyy;
  int k,t;
  int flag;
  int VAR_NUM,LEVEL_NUM;
  float tmp;
  int i,j;
  int si,ei;
  int sj,ej;
  char cmon[4];
  int gradsout;

  /*CNTL file for GrADS will be generated when gradsout=1*/
  gradsout = 0;

  if(argc < 3){
    printf("ARGV ERROR: infile outfile [slon] [elon] [slat] [elat]\n");
    exit(1);
  }else if(argc == 3){
    strcpy(infile,argv[1]);
    strcpy(outfile,argv[2]);
    slon = 0.0;
    slat = 0.0;
    elon = -1.0;
    elat = -1.0;
  }else{
    strcpy(infile,argv[1]);
    strcpy(outfile,argv[2]);
    slon = atof(argv[3]);
    elon = atof(argv[4]);
    slat = atof(argv[5]);
    elat = atof(argv[6]);
  }

  /*check necesity of swapping */
  need_byte_swapping = auto_endian_check();
  
  /*grib2 data open*/
  fp = fopen(infile,"rb");
  if(fp == NULL){
    fprintf(stderr,"open error %s\n",infile);
    exit(1);
  }

  /*save data open*/
  fo = fopen(outfile,"wb");
  if(fp == NULL){
    fprintf(stderr,"open error %s\n",outfile);
    exit(1);
  }
  
  /*read section 0*/
  status = read_section0(fp);
  if( status == 0 ){
    fprintf( stderr, "Error at section 0\n" );
    exit(40);
  }
  
  /* read section 1 */
  status = read_section1( fp, &(g.s1) );
  if( status == 0 ){
    fprintf( stderr, "Error at section 1\n" );
    exit(40);
  }
 
 
  /*no section 2 for JMA*/

  /*read section 3*/
  status = read_section3( fp, &(g.s3) );
  if( status == 0 ){
    fprintf( stderr, "Error at section 3\n" );
    exit(40);
  }
  

  if(check_radar(fp) ==0 ){
    printf("stop at check_radar \n");
    exit(1);
  }
  
  /* allocation */
  g.rrain = (float *) malloc( sizeof(float) * g.s3.nx * g.s3.ny );
  if( g.rrain == NULL ){
    fprintf( stderr, "allocation error\n" );
    exit(45);
  }

  /* read section 4*/
  status = read_section4( fp, &g );
  if( status == 0 ){
    fprintf( stderr, "Error at section 4\n" );
    exit(50);
  }  

  /* read section 5 */
  status = read_section5( fp, &(g.s5) );
  if( status == 0 ){
    fprintf( stderr, "Error at section 5\n" );
    exit(50);
  }

  /* read section 6 */
  status = read_section6( fp );
  if( status == 0 ){
    fprintf( stderr, "Error at section 6\n" );
    exit(60);
  }
  
  /* read section 7 */
  status = read_section7( fp, &g );
  if( status == 0 ){
    fprintf( stderr, "Error at section 7\n" );
    exit(60);
  }

  /*convert the direction from south to north*/
  for(j=0;j<g.s3.ny/2;j++){
    for(i=0;i<g.s3.nx;i++){
      tmp = g.rrain[j*g.s3.nx+i];
      g.rrain[j*g.s3.nx+i] = g.rrain[(g.s3.ny-j-1)*g.s3.nx+i];
      g.rrain[(g.s3.ny-j-1)*g.s3.nx+i] = tmp;
    }
  }

  
  /*region selection*/
  if(slon > g.s3.x0){
    si = (int)((slon-g.s3.x0)/g.s3.dx)+1;
  }else{
    si = 0;
  }

  if(elon < slon){
    ei = g.s3.nx;
  }else if(elon <= g.s3.lon[g.s3.nx-1]){
    ei = (int)((elon-g.s3.x0)/g.s3.dx)+1;
  }else{
    ei = g.s3.nx;
  }

  if(slat > g.s3.y0){
    sj = (int)((slat-g.s3.y0)/g.s3.dy)+1;
  }else{
    sj = 0;
  }

  if(elat < slat){
    ej = g.s3.ny;
  }else if(elat <= g.s3.lat[g.s3.ny-1]){
    ej = (int)((elat-g.s3.y0)/g.s3.dy)+1;
  }else{
    ej = g.s3.ny;
  }

  printf("si=%d ei=%d sj=%d ej=%d \n",si,ei-1,sj,ej-1);
  printf("slon=%f elon=%f slat=%f elat=%f \n",g.s3.lon[si],g.s3.lon[ei-1],
	 g.s3.lat[sj],g.s3.lat[ej-1]);
  printf("nx=%d ny=%d \n",ei-si,ej-sj);
    
  nxx = ei-si;
  nyy = ej-sj;
  for(j=sj;j<ej;j++){
    fwrite(&(g.rrain[j*g.s3.nx+si]),sizeof(float),nxx,fo);
  }

  fclose(fp);
  fclose(fo);

  if(gradsout == 1){
    fp = fopen("view_radar.ctl","w");
    fprintf(fp,"dset %s \n",outfile);
    fprintf(fp,"undef -999.9\n");
    fprintf(fp,"xdef %d LINEAR %f %f\n",nxx,g.s3.lon[si],g.s3.dx);
    fprintf(fp,"ydef %d LINEAR %f %f\n",nyy,g.s3.lat[sj],g.s3.dy);
    fprintf(fp,"zdef 1  LEVELS 1000\n");
    
    if(g.s1.imon == 1){
      strcpy(cmon,"Jan");
    }else if(g.s1.imon == 2){
      strcpy(cmon,"Feb");
    }else if(g.s1.imon == 3){
      strcpy(cmon,"Mar");
    }else if(g.s1.imon == 4){
      strcpy(cmon,"Apl");
    }else if(g.s1.imon == 5){
      strcpy(cmon,"May");
    }else if(g.s1.imon == 6){
      strcpy(cmon,"Jun");
    }else if(g.s1.imon == 7){
      strcpy(cmon,"Jul");
    }else if(g.s1.imon == 8){
      strcpy(cmon,"Aug");
    }else if(g.s1.imon == 9){
      strcpy(cmon,"Sep");
    }else if(g.s1.imon == 10){
      strcpy(cmon,"Oct");
    }else if(g.s1.imon == 11){
      strcpy(cmon,"Nov");
    }else if(g.s1.imon == 12){
      strcpy(cmon,"Dec");
    }
    
    fprintf(fp,"tdef 1  LINEAR %02d:%02dZ%02d%c%c%c%04d 10mn\n",
	    g.s1.ihour,g.s1.imin,g.s1.iday,cmon[0],cmon[1],cmon[2],
	    g.s1.iyear);
    fprintf(fp,"vars 1\n");
    fprintf(fp,"rr 0 0 rainfall\n");
    fprintf(fp,"endvars\n");
    
    fclose(fp);
  }

}
