#include <stdio.h>
#include <math.h>
#include "grib2.h"
#include <string.h>
#include <stdlib.h>


int read_section5( FILE *fp, struct grib2_section5 *s5)
{
  
  unsigned char buf[519];
  int tmp;
  int test;
  short V;
  short M;
  short tmp2;
  int m;
  int scale_factor;

  if( fread( buf, 1, 519, fp ) != 519)
    return(0);

  test = swapping( buf, it_int );

  
  if( buf[4] != 5 ) /* section number */
    return(0);

  s5->datanum = swapping(buf+5,it_int);
  
  s5->Vbit = (int)(buf[11]);
  printf("bits = %d \n",s5->Vbit);
  
  s5->VMAX = (int)(swapping(buf+12,it_short));
  printf("V = %d \n",s5->VMAX);
  
  s5->level = (int)(swapping(buf+14,it_short));
  printf("level = %d \n",s5->level);
  
  scale_factor = (int)(buf[16]);
  printf("scale_factor= %d \n",scale_factor);
  
  s5->RR = (void*)malloc(sizeof(float)*s5->level);
  
  for(m=0;m<s5->level;m++){
    //    s5->RR[m] = (float)(swapping(buf+17+2*m,it_short)) / pow(10.0,scale_factor);
    s5->RR[m] = (float)(swapping(buf+15+2*m,it_short)) / pow(10.0,scale_factor);
     printf("m= %d R=%f \n",m,s5->RR[m]);
  }

  return(1);

}
