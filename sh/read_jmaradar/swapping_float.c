#include "grib2.h"
 
float swapping_float( void *p )
{
  union{
    unsigned char c[4];
    float f;
  } u;
  
  
  if( need_byte_swapping ){
    u.c[0] = *( (unsigned char *)p + 3 );
    u.c[1] = *( (unsigned char *)p + 2 );
    u.c[2] = *( (unsigned char *)p + 1 );
    u.c[3] = *( (unsigned char *)p + 0 );
  }
  else{
    u.c[0] = *( (unsigned char *)p + 0 );
    u.c[1] = *( (unsigned char *)p + 1 );
    u.c[2] = *( (unsigned char *)p + 2 );
    u.c[3] = *( (unsigned char *)p + 3 );
  }
  
  return( u.f );
  
}
  
