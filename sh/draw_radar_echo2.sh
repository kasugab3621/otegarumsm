#!/bin/bash
#--------------------------------------------------------------------
# draw JMA combined radar data
#      ( dBZ in 10min & cumulative rain ) 
#                                    Edit by Yusuke KIMURA 2014/12/07
#--------------------------------------------------------------------

PARMF="../parm.sh"

. $PARMF

STEP=draw_radar_echo

## Initialization
XDEF=`cat view_radar.ctl | grep xdef`
YDEF=`cat view_radar.ctl | grep ydef`
#FNUM=`ls -l $RDDIR/*.bin | wc -l`

FNUM=$FNUM10m

case $MMS in
	01 ) MON="JAN" ;;
	02 ) MON="FEB" ;;
	03 ) MON="MAR" ;;
	04 ) MON="APR" ;;
	05 ) MON="MAY" ;;
	06 ) MON="JUN" ;;
	07 ) MON="JUL" ;;
	08 ) MON="AUG" ;;
	09 ) MON="SEP" ;;
	10 ) MON="OCT" ;;
	11 ) MON="NOV" ;;
	12 ) MON="DEC" ;;
esac


## Make ctl-file
cat << EOF > ${CTLDIR}/view_radar_echo.ctl
dset ${RDDIR}/%y4%m2%d2%h2%n2.bin 
undef 0
options template
${XDEF}
${YDEF}
zdef   1 LEVELS 1000
tdef $FNUM LINEAR ${HHS}:${NNS}Z${DDS}${MON}${YYS} 10mn
vars 1
rr 0 0 ** rainfall
endvars
EOF


## Make gs-file
cat << EOF > ${GSDIR}/draw_radar_echo.gs
** Draw radar_echo...
** 2014/12/07 Corded by Yusuke KIMURA

eps=$EPS
density=600
size=600

*## Parameter
*++ 10min
CLEV1="-levs 0.5 1 2 3 5 10 15 20 30 50 80"
CKIND1="-kind white-(0.5)->rainbow"
*++ int
CLEV2="-levs 1 2 5 10 15 20 25 30 40 50 60 80"
CKIND2="-kind white-(1)->rainbow"


*## Initialization
'!mkdir -p ${IMGDIR}/radar_echo/10min'
'reinit'
'open ../ctl/view_radar_echo.ctl'
'set grads off';'c';
'set xlopts 1 5 0.15'
'set ylopts 1 5 0.15'
'set map 1 1 7'
'set mpdset $mpdset'
'set lon $LONSI $LONEI'
'set lat $LATSI $LATEI'


*## 10min Pricipitation Intensity
it=1
while(it<=${FNUM})
it9=it+54
'set t 'it9
'q dim'
line=sublin(result,5)
jst=subwrd(line,6)
coron=substr(jst,3,1)
if(coron=":")
hour=substr(jst,1,2)
min=substr(jst,4,2)
day=substr(jst,7,2)
emon=substr(jst,9,3)
year=substr(jst,12,4)
else
min="00"
hour=substr(jst,1,2)
day=substr(jst,4,2)
emon=substr(jst,6,3)
year=substr(jst,9,4)
endif

if(emon=JAN);nmon="01";endif
if(emon=FEB);nmon="02";endif
if(emon=MAR);nmon="03";endif
if(emon=APR);nmon="04";endif
if(emon=MAY);nmon="05";endif
if(emon=JUN);nmon="06";endif
if(emon=JUL);nmon="07";endif
if(emon=AUG);nmon="08";endif
if(emon=SEP);nmon="09";endif
if(emon=OCT);nmon="10";endif
if(emon=NOV);nmon="11";endif
if(emon=DEC);nmon="12";endif

tdate=year'/'nmon'/'day' 'hour':'min
if(it=1);tsdate=tdate;endif
if(it=${FNUM});tedate=tdate;endif
say tdate

   'set t 'it
   'color 'CLEV1' 'CKIND1
   'd rr'
   'cbar'
   'draw title otgrRadar-Echo 10min Pricip-Intensity [mm/hr] \ 'tdate
if(eps=1)
   'print -R tmp.eps'
   '!convert -density 'density' -resize 'size' +antialias tmp.eps ${IMGDIR}/radar_echo/10min/RadarEcho_'year''nmon''day''hour''min'.png'
else
   'printim ${IMGDIR}/radar_echo/10min/RadarEcho_'year''nmon''day''hour''min'.png x1100 y850 white'
endif
'set grads off';'c';'set grads off'
   it = it + 1
endwhile


*## integrated Pricipitation
say tsdate' - 'tedate
   'color 'CLEV2' 'CKIND2
   'd sum(rr,t=1,t=$FNUM)/6'
   'cbar'
   'draw title Radar-Echo Pricipitation [mm] \ 'tsdate' - 'tedate
if(eps=1)
   'print -R tmp.eps'
   '!convert -density 'density' -resize 'size' +antialias tmp.eps ${IMGDIR}/radar_echo/RadarEcho_int-Pricip.png'
else
   'printim ${IMGDIR}/radar_echo/RadarEcho_int-Pricip.png x1100 y850 white'
endif

'quit'

EOF


## Run the gs-file
cd ${GSDIR}
grads -blc 'draw_radar_echo.gs' #1>/dev/null 2>/dev/null

cd ${CRTDIR}
exit

CHECK=`which convert | wc -l`
if [ $CHECK -eq 1 ]; then
    cd ${IMGDIR}/radar_echo/.
    convert -loop 0 -delay 30 10min/*.png anime.gif
fi


