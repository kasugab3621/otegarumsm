#! /bin/bash

. ../parm.sh
STEP=calc_cape.sh

#memo
#undef msm=9.999e20, msm_topo=-999.9, grads(def) -9.99e8
undef=9.999e20

#if [ 1 = 2 ];then

for var in TMP RH UGRD VGRD ; do
    case $var in
	TMP|RH) suf=2m ;;
	UGRD|VGRD) suf=10m ;;
    esac

fort/p2z-m10m.exe <<EOF
$FNUM3h
241,253,481,505,16,21
1000,975,950,925,900,850,800,700,600,500,400,300,250,200,150,100
0,500,1000,1500,2000,2500,3000,3500,4000,4500,5000,5500,6000,6500,7000,7500,8000,8500,9000,9500,10000
$undef
'$MSMBIN'
'$TOPO'
'HGTprs.bin','PRESsfc.bin'
'${var}prs.bin','${var}${suf}.bin'
'${var}z.bin','PRESz.bin'
EOF

done

fort/rh2shz.exe <<EOF
$FNUM3h
241,253,21
$undef
'$MSMBIN'
'RHz.bin','TMPz.bin','PRESz.bin'
'SPFHz.bin','SSFPHz.bin'
EOF

fort/eqptz-s.exe <<EOF
$FNUM3h
241,253,21
0,500,1000,1500,2000,2500,3000,3500,4000,4500,5000,5500,6000,6500,7000,7500,8000,8500,9000,9500,10000
$undef
'$MSMBIN'
'TMPz.bin','SPFHz.bin','PRESz.bin'
'EQPTz.bin','ZLCLz.bin','SEQPTz.bin'
EOF

# for MLCAPE
####################
fort/meanlayer.exe <<EOF
$FNUM3h
241,253,21
$undef
'$MSMBIN'
'TMPz.bin','SPFHz.bin','PRESz.bin'
'TMPmlz.bin','SPFHmlz.bin','PRESmlz.bin'
EOF

fort/eqptz-s.exe <<EOF
$FNUM3h
241,253,1
500
$undef
'$MSMBIN'
'TMPmlz.bin','SPFHmlz.bin','PRESmlz.bin'
'EQPTmlz.bin','ZLCLmlz.bin','SEQPTmlz.bin'
EOF
####################


# first lifting
fort/liftlayers.exe <<EOF 
$FNUM3h
241,253,21
0,500,1000,1500,2000,2500,3000,3500,4000,4500,5000,5500,6000,6500,7000,7500,8000,8500,9000,9500,10000
$undef
'$MSMBIN'
'TMPz.bin','EQPTz.bin','SEQPTz.bin','PRESz.bin','ZLCLz.bin'
'EQPTmlz.bin','ZLCLmlz.bin',
'__TO2sbz.bin','__TO25z.bin','__TO2muz.bin','__TO2mlz.bin'
EOF
####


#loop for lifting type
for var in sb 5 mu ml ; do
#for var in ml ; do

#2nd eqpt & cape
to2bin="__TO2"$var"z.bin"
seqpt2bin="__SEQPT2"$var"z.bin"

#if [ 1 = 2 ];then
fort/eqptz-s.exe <<EOF 
$FNUM3h
241,253,21
0,500,1000,1500,2000,2500,3000,3500,4000,4500,5000,5500,6000,6500,7000,7500,8000,8500,9000,9500,10000
$undef
'$MSMBIN'
'$to2bin','SPFHz.bin','PRESz.bin'
'__dummy1.bin','__dummy2.bin','$seqpt2bin'
EOF
#fi

#2nd(final) cape
ncape="CAPE"$var
nto3="TO3"$var"z"

if [ $var = "ml" ] ;then
    ilay=1
else
    ilay=21
fi

fort/capev2.exe <<EOF
$FNUM3h
241,253,21,$ilay
0,500,1000,1500,2000,2500,3000,3500,4000,4500,5000,5500,6000,6500,7000,7500,8000,8500,9000,9500,10000
$undef
'$MSMBIN'
'$to2bin','TMPz.bin'
'EQPTz.bin','SPFHz.bin','$seqpt2bin'
'PRESz.bin','ZLCLz.bin'
'${ncape}.bin','${nto3}.bin'

EOF

tdefl=`cat $MSMBIN/HGTprs.ctl | grep tdef`
tgrads=`echo $tdefl | cut -d " " -f 4`
it=`echo $tdefl | cut -d " " -f 2`

cat << EOF > $MSMBIN/$ncape.ctl
dset ^$ncape.bin
undef 9.999E+20
title $ncape
options big_endian
xdef 241 linear 120.000000 0.125000
ydef 253 linear 22.400000 0.1000
tdef $it linear $tgrads 3hr
zdef 1 levels 1
vars 7
cape 1 0 **
cin 1 0 **
lfc 1 0 **
lnb 1 0 **
lcl 1 0 **
hgtos 1 0 **
presos 1 0 **
ENDVARS
EOF

cat << EOF > $MSMBIN/$nto3.ctl
dset ^$nto3.bin
undef 9.999E+20
title $nto3
options big_endian
xdef 241 linear 120.000000 0.125000
ydef 253 linear 22.400000 0.1000
tdef $it linear $tgrads 3hr
zdef 21 levels 0 500 1000 1500 2000 2500 3000 3500 4000 4500 5000 5500 6000 6500 7000 7500 8000 8500 9000 9500 10000
vars 1
to3 21 0 **
ENDVARS
EOF

done
exit

rm -rf $MSMBIN/__*.bin

