*====================================================================
* 指定されている時刻を読み込んで、年、月、日、時、分の情報を求める関数
* '14.05.22 Corded by Yusuke KIMURA 
*====================================================================
function get_time()

* If you want the JST date, $flag_jst = 1.
flag_jst = 1


* Get time info
*====================================================================
'q dim'
lin = sublin ( result, 5 )
date = subwrd ( lin, 6 )
*se  = subwrd ( lin, 8 )


* Input year,month,day,hour,minute
*====================================================================
check = substr ( date, 3, 1 )
if ( check = 'Z' )
*   say 'THIS IS Z.'
   year = substr( date, 9, 4 )
   cmon = substr( date, 6, 3 )
   day  = substr( date, 4, 2 )
   hour = substr( date, 1, 2 )
   min  = '00'
else
*   say 'THIS IS :.'
   year = substr( date, 12, 4 )
   cmon = substr( date, 9, 3 )
   day  = substr( date, 7, 2 )
   hour = substr( date, 1, 2 )
   min  = substr( date, 4, 2 )
endif


year = year + 0
day  = day + 0
hour = hour + 0
*min  = min + 0

* CAL to NUM
*====================================================================
if ( cmon = 'JAN' ); nmon =  '1' ; endif
if ( cmon = 'FEB' ); nmon =  '2' ; endif
if ( cmon = 'MAR' ); nmon =  '3' ; endif
if ( cmon = 'APR' ); nmon =  '4' ; endif
if ( cmon = 'MAY' ); nmon =  '5' ; endif
if ( cmon = 'JUN' ); nmon =  '6' ; endif
if ( cmon = 'JUL' ); nmon =  '7' ; endif
if ( cmon = 'AUG' ); nmon =  '8' ; endif
if ( cmon = 'SEP' ); nmon =  '9' ; endif
if ( cmon = 'OCT' ); nmon = '10' ; endif
if ( cmon = 'NOV' ); nmon = '11' ; endif
if ( cmon = 'DEC' ); nmon = '12' ; endif


* UTC to JST
*====================================================================
flag2=0
if ( flag_jst = 1 )
   hour = hour + 9
endif
if ( hour > 23 )
   hour = hour - 24
   day  = day + 1      
endif
if ( nmon = 2 )
   rmnd = math_fmod( year, 4 )
*   say rmnd
   if ( rmnd>0 ) & ( day > 28 )
      day = '1'
      nmon = 3
   endif
   if ( rmnd=0 ) & ( day > 29 )
      day = '1'
      nmon = 3
   endif
endif
if (nmon=4 | nmon=6 | nmon=9 | nmon=11) & (day>30)
   day = '1'
   nmon = nmon + 1
endif
if (nmon=1 | nmon=3 | nmon=5 | nmon=7 | nmon=8 | nmon=10 | nmon=12) & (day>31)
*   say hoge
   day = '1'
   nmon = nmon + 1
endif
if ( nmon > 12 )
   nmon = 1
   year = year + 1
endif

* printf(%d2) [month,day,hour]
*====================================================================
if ( nmon < 10 ); nmon = '0'nmon ; endif
if ( day < 10 ) ;  day = '0'day  ; endif
if ( hour < 10 ); hour = '0'hour ; endif


* Output
*====================================================================
*say date
*say 'Echo from get_time.gs'
say year'/'nmon'/'day' 'hour':'min

dum=write(TIMECARD,year)
dum=write(TIMECARD,nmon,append)
dum=write(TIMECARD,day,append)
dum=write(TIMECARD,hour,append)
dum=write(TIMECARD,min,append)
dum=close(TIMECARD)

flag1=0
if (flag1=1)
   say year
   say cmon
   say day
   say hour
   say min
   say test1 test2 test3 test4 test5 test6
endif
*


return
*====================================================================

