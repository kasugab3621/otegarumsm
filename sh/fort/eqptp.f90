!!===================================================================
!! 相当温位を計算するプログラム(p座標系)
!! x,y,p cordinate
!! input  :tmp,spfh,hgt
!! output :eqpt,zlcl
!!===================================================================
!! 2015/04/07 Corded by Yusuke KIMURA
!! 2016/10/07 kasuga
!!===================================================================
program main
  implicit none
  integer             :: ilon,ilat,ihgt,itime
  real(4)             :: undef
  real(4),allocatable :: data1(:,:,:),data2(:,:,:),data3(:,:,:)
  real(4),allocatable :: qv(:,:,:),qv2(:,:,:),es(:,:,:)
  real(4),allocatable :: pmap(:,:,:),pres(:)
  real(4),allocatable :: tlcl(:,:,:),zlcl(:,:,:)
  real(4),allocatable :: dz(:,:,:),hgt(:,:,:)
  real(4),allocatable :: eqpt(:,:,:)
  character(150)      :: input1,input2,input3
  character(150)      :: output1,output2,path
  integer :: i,j,k,it

  !## read infile (1)
  read(5,*)itime
  read(5,*)ilon,ilat,ihgt
  
  !## initialization
  allocate( data1(ilon,ilat,ihgt), &
            data2(ilon,ilat,ihgt), &
            qv(ilon,ilat,ihgt),    &
            qv2(ilon,ilat,ihgt),   &
            es(ilon,ilat,ihgt),    &
            pmap(ilon,ilat,ihgt),  &
            tlcl(ilon,ilat,ihgt),  &
            eqpt(ilon,ilat,ihgt),  &
            pres(ihgt),  &
            zlcl(ilon,ilat,ihgt),  &
            dz(ilon,ilat,ihgt),  &
            hgt(ilon,ilat,ihgt)    &
            )

  !## read infile (2)
  read(5,*) pres
  read(5,*) undef
  read(5,*) path
  read(5,*) input1,input2,input3
  read(5,*) output1,output2
            
  !## 気圧を3次元データとして保持
  do k=1,ihgt
     pmap(:,:,k) = pres(k)
  end do
  
  !## open
  !++ 気温(data1)
  open (11, file=trim(path)//"/"//input1,status='old',&
       form='unformatted',access='direct',recl=ilon*ilat*ihgt*4)
  !++ 比湿(data2)
  open (12, file=trim(path)//"/"//input2,status='old',&
       form='unformatted',access='direct',recl=ilon*ilat*ihgt*4)
  !++ 気圧
  open (13, file=trim(path)//"/"//input3,status='old',&
       form='unformatted',access='direct',recl=ilon*ilat*ihgt*4)
  !++ 相当温位
  open (21, file=trim(path)//"/"//output1,status='unknown',&
       form='unformatted',access='direct',recl=ilon*ilat*ihgt*4)
  !++ 持ち上げ凝結高度
  open (22, file=trim(path)//"/"//output2,status='unknown',&
       form='unformatted',access='direct',recl=ilon*ilat*ihgt*4)

  !## 計算開始
  write(6,*) 'start!'
  do it=1,itime
     write(6,*) it
     read(11,rec=it) data1 !! temperature
     read(12,rec=it) data2 !! Specific Humidity
     read(13,rec=it) hgt !! hight

     !## 持ち上げ凝結高度を計算(add dz out.)
     call cal_tlcl(ilon,ilat,ihgt,undef,data1,data2,pmap,dz,tlcl)
     !## 相当温位を計算
     call cal_eqpt(ilon,ilat,ihgt,undef,data1,tlcl,pmap,data2,eqpt)

     
     !! 計算した相当温位をファイル出力
     write(21,rec=it) eqpt

     !! 全グリッドにおけるLCL(地面からの距離)
     do k=1,ihgt
        do j=1,ilat
           do i=1,ilon
              if(dz(i,j,k).eq.undef) then
                 zlcl(i,j,k)=undef
              else
                 zlcl(i,j,k)= hgt(i,j,k) + dz(i,j,k)
                 !zlcl(i,j,k)= dz(i,j,k)
              end if
           end do
        end do
     end do
     write(22,rec=it) zlcl
  enddo

end program main


!!##################################################################
!! 吉崎&加藤の方法を用いて持ち上げ凝結高度における気温を
!! 計算するサブルーチン
!! 2015/04/02 Edit by Yusuke KIMURA
!!##################################################################
! 加藤＆吉崎(2007)の式に各変数を引き渡す
!     ...「豪雨・豪雪の気象学」 p164 参照
!  +-- tlcl : 持ち上げ凝結高度における凝結温度 K
!  +-- to   : 持ち上げ前の温度 K
!  +-- qvso : 持ち上げ前の飽和水蒸気混合比 kg/kg
!  +-- qv   : 持ち上げ前の水蒸気混合比 kg/kg
!  +-- Cpd  : 乾燥空気の定圧比熱 J / K / kg
!  +-- Rd   : 乾燥空気の気体定数 J / K / kg
!  +-- Lv   : 水から水蒸気への蒸発熱 J / kg
!  +-- eps  : epsilon(ε) 水蒸気と乾燥空気の気体定数の比
!# Tetens's Formula
!  +-- es   : 飽和水蒸気圧
!####################################################################
subroutine cal_tlcl(ilon,ilat,ihgt,undef,tmp,qv,p,dz,tlcl_2)
  implicit none
  integer              :: i,j,k
  integer, intent(in)  :: ilon,ilat,ihgt
  real(4), intent(in)  :: undef
  real(4), intent(in)  :: tmp(ilon,ilat,ihgt)
  real(4), intent(in)  :: qv(ilon,ilat,ihgt)
  real(4), intent(in)  :: p(ilon,ilat,ihgt)
  real(4), intent(out) :: tlcl_2(ilon,ilat,ihgt)
  real(4), intent(out) :: dz(ilon,ilat,ihgt)
  real(4)              :: p2(ilon,ilat,ihgt)
  real(4)              :: qvso(ilon,ilat,ihgt), es(ilon,ilat,ihgt)
  real(4)              :: dp(ilon,ilat,ihgt)
  real(4)              :: tlcl_1(ilon,ilat,ihgt)
  
  !## 1st : to, qv --> tlcl'
  !++ Tetens's Formula
  call tetens_tmp2es(ilon,ilat,ihgt,undef,tmp,es)
  !++ Saturated mixing ratio
  call ep2qv(ilon,ilat,ihgt,undef,es,p,qvso)
  !++ Yoshizaki & Kato's Formula
  call tmpqvqvs2tlcl(ilon,ilat,ihgt,undef,tmp,qv,qvso,tlcl_1)

  !## 2nd : tlcl', qvso_tlcl --> tlcl'
  !++ dT --> dz
  call dt2dz(ilon,ilat,ihgt,undef,tmp,tlcl_1,dz)
  !++ dz --> dp
  call dz2dp(ilon,ilat,ihgt,undef,dz,dp)
  !++ p <= p + dp
  do k=1,ihgt
     do j=1,ilat
        do i=1,ilon
           if ( p(i,j,k).ne.undef .and. dp(i,j,k).ne.undef ) then
              p2(i,j,k) = p(i,j,k) + dp(i,j,k)
           else
              p2(i,j,k) = undef
           end if
        end do
     end do
  end do
  !++ Tetens's Formula
  call tetens_tmp2es(ilon,ilat,ihgt,undef,tlcl_1,es)
  !++ Saturated mixing ratio
  call ep2qv(ilon,ilat,ihgt,undef,es,p2,qvso)
  !++ Yoshizaki & Kato's Formula
  call tmpqvqvs2tlcl(ilon,ilat,ihgt,undef,tlcl_1,qv,qvso,tlcl_2)
  !++ for zlcl
  call dt2dz(ilon,ilat,ihgt,undef,tmp,tlcl_2,dz)
  
!  write(6,*) "2st. Tlcl, qvos_lcl --> tlcl "
!  write(6,"(A10,F13.5)") "dz   : ", dz(1,1,1)
!  write(6,"(A10,F13.5)") "dp   : ", dp(1,1,1)
!  write(6,"(A10,F13.5)") "PLEV : ", p2(1,1,1)
!  write(6,"(A10,F13.5)") "TMP  : ", tlcl_1(1,1,1)
!  write(6,"(A10,F13.5)") "qv   : ", qv(1,1,1)
!  write(6,"(A10,F13.5)") "qvso : ", qvso(1,1,1)
!  write(6,"(A10,F13.5)") "Tlcl : ",tlcl_2(1,1,1)
end subroutine cal_tlcl


!####################################################################
! 加藤＆吉崎(2007)の式から持ち上げ凝結高度を求めるサブルーチン
!     ...「豪雨・豪雪の気象学」 p164 付録A-3参照
!####################################################################
subroutine tmpqvqvs2tlcl(ilon,ilat,ihgt,undef,tmp,qv,qvso,tlcl)
  implicit none
  integer, intent(in)  :: ilon,ilat,ihgt
  real(4), intent(in)  :: undef
  real(4), intent(in)  :: tmp(ilon,ilat,ihgt)
  real(4), intent(in)  :: qv(ilon,ilat,ihgt)
  real(4), intent(in)  :: qvso(ilon,ilat,ihgt)
  real(4), intent(out) :: tlcl(ilon,ilat,ihgt)
  real(4), parameter   :: Cpd = 1004.d0
  real(4), parameter   :: Rd  = 287.d0
  real(4), parameter   :: Lv  = 2.500E+6
  real(4), parameter   :: eps = 0.622
  integer              :: i,j,k
  real(4)              :: A,B,C,ROOT  


  do k=1,ihgt
     do j=1,ilat
        do i=1,ilon
           if ( tmp(i,j,k) .ne.undef .and. &
                qv(i,j,k)  .ne.undef .and. &
                qvso(i,j,k).ne.undef ) then
              A = 2 * (qvso(i,j,k)-qv(i,j,k)) / (qvso(i,j,k)+qv(i,j,k))
              B = Cpd*( eps + ((qvso(i,j,k)+qv(i,j,k))/2) ) / ( eps*Rd )
              C = Lv * ( eps + ((qvso(i,j,k)+qv(i,j,k))/2) ) / Rd
              ROOT = sqrt( (B*tmp(i,j,k)-C)**2 + 2*A*C*tmp(i,j,k) )
              tlcl(i,j,k) = &
                   2 * (B*tmp(i,j,k) + C - ROOT) / (2*B - A) - tmp(i,j,k)
           else
              tlcl(i,j,k) = undef
           end if
        end do
     end do
  end do

end subroutine tmpqvqvs2tlcl


!!###################################################################
!! Bolton(1980)の式から相当温位を計算するサブルーチン
!! 2015/04/07 Corded by Yusuke KIMURA
!!###################################################################
subroutine cal_eqpt(ilon,ilat,ihgt,undef,tmp,tlcl,p,spfh,eqpt)
  implicit none
  integer, intent(in)  :: ilon,ilat,ihgt
  real(4), intent(in)  :: undef
  real(4), intent(in)  :: tmp(ilon,ilat,ihgt)
  real(4), intent(in)  :: tlcl(ilon,ilat,ihgt)
  real(4), intent(in)  :: p(ilon,ilat,ihgt)
  real(4), intent(in)  :: spfh(ilon,ilat,ihgt)
  real(4), intent(out) :: eqpt(ilon,ilat,ihgt)
  real(4), parameter   :: Cpd = 1004.d0
  real(4), parameter   :: Rd  = 287.d0
  real(4), parameter   :: Lv  = 2.500E+6
  real(4), parameter   :: eps = 0.622
  real(4), parameter   :: p0  = 1000.d0 
  integer              :: i, j, k
  real(4)              :: es_lcl(ilon,ilat,ihgt)
  real(4)              :: qv(ilon,ilat,ihgt)
  real(4)              :: pt, exp1,exp2
  
  !! Tlcl --> es_lcl
  call tetens_tmp2es(ilon,ilat,ihgt,undef,tlcl,es_lcl)
  !! spfh --> qv
  call spfh2qv(ilon,ilat,ihgt,undef,spfh,qv)
  
  !! Calculate Equivalent Potential Temperature
  do k=1,ihgt
     do j=1,ilat
        do i=1,ilon
           if ( tmp(i,j,k)   .ne. undef .and. &
                tlcl(i,j,k)  .ne. undef .and. &
                p(i,j,k)     .ne. undef .and. &
                es_lcl(i,j,k).ne. undef .and. &
                qv(i,j,k)    .ne. undef ) then
              pt   = tmp(i,j,k) * ( p0/(p(i,j,k)-es_lcl(i,j,k)) ) ** (Rd/Cpd)
              exp1 = (3036.d0/tlcl(i,j,k) - 1.78)
              exp2 = 1 + 0.448 * qv(i,j,k)
              eqpt(i,j,k) = pt * exp( exp1 * qv(i,j,k) * exp2 )
           else
              eqpt(i,j,k) = undef
           endif
        end do
     end do
  end do

!  write(6,*) "-- @ cal_eqpt --"
!  write(6,"(A10,F13.5)") "TMP  : ", tmp(1,1,1)
!  write(6,"(A10,F13.5)") "Tlcl : ", tlcl(1,1,1)
!  write(6,"(A10,F13.5)") "P    : ", p(1,1,1)
!  write(6,"(A10,F13.5)") "qv   : ", qv(1,1,1)
!  write(6,"(A10,F13.5)") "elcl : ", es_lcl(1,1,1)
!  write(6,"(A10,F13.5)") "eqpt : ", eqpt(1,1,1)

  
end subroutine cal_eqpt

!!##################################################################
!! 比湿を混合比に変換するサブルーチン
!! 2015/04/02 Edited by Yusuke KIMURA
!!##################################################################
subroutine spfh2qv(ilon,ilat,ihgt,undef,spfh,qv)
  implicit none
  integer,intent(in)  :: ilon,ilat,ihgt
  real(4),intent(in)  :: undef
  real(4),intent(in)  :: spfh(ilon,ilat,ihgt)
  real(4),intent(out) :: qv(ilon,ilat,ihgt)
  integer :: i,j,k

!  write(6,*) "++ Subroutine : spfh2qv ++"
!  write(6,*) "   Run..."
  do k=1,ihgt
     do j=1,ilat
        do i=1,ilon
           if( spfh(i,j,k).ne.undef )then
              qv(i,j,k) = spfh(i,j,k) / ( 1.d0 - spfh(i,j,k) )
           else
              qv(i,j,k) = undef
           end if
        end do
     end do
  end do
!  write(6,*) "      -->End."
!  write(6,*) "++ -------------------- ++"
  
end subroutine spfh2qv


!!##################################################################
!! Tetensの式を用いて気温から飽和水蒸気圧を求めるサブルーチン
!! 2015/04/02 Edit by Yusuke KIMURA
!!##################################################################
subroutine tetens_tmp2es(ilon,ilat,ihgt,undef,tmp,es)
  implicit none
  integer,intent(in)  :: ilon, ilat, ihgt
  real(4),intent(in)  :: undef
  real(4),intent(in)  :: tmp(ilon,ilat,ihgt)
  real(4),intent(out) :: es(ilon,ilat,ihgt)
  integer             :: i, j, k
  real(4)             :: exptmp
!  write(6,*) "++ Subroutine : tetens_tmp2es ++"
!  write(6,*) "   Run..."

  do k=1,ihgt
     do j=1,ilat
        do i=1,ilon
           if( tmp(i,j,k) .ne. undef )then
              exptmp = exp(17.27*(tmp(i,j,k)-273.15)/(tmp(i,j,k)-35.86))  
              es(i,j,k) = 6.11 * exptmp
           else
              es(i,j,k) = undef
           end if
        end do
     end do
  end do
  
!  write(6,*) "      -->End."
!  write(6,*) "++ -------------------------- ++"
  
end subroutine tetens_tmp2es


!!##################################################################
!! 水蒸気圧と気圧から水蒸気混合比を求めるサブルーチン
!! 2015/04/02 Edit by Yusuke KIMURA
!!##################################################################
subroutine ep2qv(ilon,ilat,ihgt,undef,e,p,qv)
  implicit none
  real(4),parameter   :: epsilon = 0.622
  integer,intent(in)  :: ilon, ilat, ihgt
  real(4),intent(in)  :: undef
  real(4),intent(in)  :: e(ilon,ilat,ihgt)
  real(4),intent(in)  :: p(ilon,ilat,ihgt)
  real(4),intent(out) :: qv(ilon,ilat,ihgt)
  integer :: i,j,k
  
  do k=1,ihgt
     do j=1,ilat
        do i=1,ilon
           if( e(i,j,k).ne.undef .and. p(i,j,k).ne.undef )then
              qv(i,j,k) = ( epsilon * e(i,j,k) ) / ( p(i,j,k) - e(i,j,k) )
           else
              qv(i,j,k) = undef
           endif
        end do
     end do
  end do
  
end subroutine ep2qv


!!##################################################################
!! 乾燥断熱減率をもちいて気温差から高度差を求めるサブルーチン
!! 2015/04/02 Edit by Yusuke KIMURA
!!##################################################################
!! (T1,z1) --> (T2,z2) で乾燥断熱的に気塊が上昇した時の高度差を計算する
!! ⊿z = z2 - z1
!! ⊿T = T2 - T1
!! GAMd = - ⊿T / ⊿z
!! したがって、
!! ⊿z = - ⊿T / GAMd = -(T2 - T1) / GAMd
!!##################################################################
subroutine dt2dz( ilon,ilat,ihgt,undef,tmp1,tmp2, dz)
  implicit none
  integer, intent(in)  :: ilon,ilat,ihgt
  real(4), intent(in)  :: undef
  real(4), intent(in)  :: tmp1(ilon,ilat,ihgt), tmp2(ilon,ilat,ihgt)
  real(4), intent(out) :: dz(ilon,ilat,ihgt)
  real(4), parameter   :: gamd = 9.8000E-3
  integer              :: i,j,k
  
  do k=1,ihgt
     do j=1,ilat
        do i=1,ilon
           if ( (tmp1(i,j,k).ne.undef) .and. &
                (tmp2(i,j,k).ne.undef) ) then
              dz(i,j,k) = - (tmp2(i,j,k) - tmp1(i,j,k)) / gamd
           else
              dz(i,j,k) = undef
           end if
        end do
     end do
  end do
  
end subroutine dt2dz


!!##################################################################
! 静水圧平衡の式をもちいて、高度差[m]から気圧差[hPa]を求めるプログラム
! 2015/04/02 Edited by Yusuke KIMURA
!!##################################################################
subroutine dz2dp(ilon,ilat,ihgt,undef,dz,dp)
  implicit none
  real(4),parameter   :: roh=1.d0
  real(4),parameter   :: g=9.8d0
  integer,intent(in)  :: ilon,ilat,ihgt
  real(4),intent(in)  :: undef
  real(4),intent(in)  :: dz(ilon,ilat,ihgt)
  real(4),intent(out) :: dp(ilon,ilat,ihgt)
  integer             :: i,j,k

  do k=1,ihgt
     do j=1,ilat
        do i=1,ilon
           if ( dz(i,j,k) .ne. undef ) then
              dp(i,j,k) = ( -1 * roh * g * dz(i,j,k) ) / 100
           end if
        end do
     end do
  end do
  
end subroutine dz2dp

!! 以下、サブルーチンの単体テスト用コード
!!###################################################################  
!  !++ test.1 : spfh2qv
!  call spfh2qv(ilon,ilat,ihgt,undef,data2,qv)
!  !++ test.2 : tetens_tmp2es
!  call tetens_tmp2es(ilon,ilat,ihgt,undef,data1,es)
!  !++ test.3 : ep2qv
!  call ep2qv(ilon,ilat,ihgt,undef,es,pmap,qv2)
  
!  do k=ihgt,1,-1
!     write(6,"(I3,2F12.3)") k, data1(1,1,k)-273.15, data2(1,1,k)*1000
!     write(6,"(F9.1,2F12.3)") plev(k), data1(1,1,k)-273.15, data2(1,1,k)*1000
!     write(6,"(F9.1,5F12.6)") &
!          plev(k), data1(1,1,k)-273.15, data2(1,1,k)*1000, &
!          qv(1,1,k)*1000,qv2(1,1,k)*1000,es(1,1,k)
!  enddo

!  tmp1(1,1,1)=0.d0
!  tmp2(1,1,1)=-9.8d0
!  !++ test.4 : dt2dz
!  call dt2dz(1,1,1,undef,tmp1,tmp2,dz)
!  !++ test.5 : dz2dp
!  dz(1,1,1)=1500
!  call dz2dp(1,1,1,undef,dz,dp)
!  write(6,*) tmp2-tmp1, dz, dp 
!!###################################################################  
  



     !! 計算結果の検証用出力部分
!     write(6,"(A57)") &
!          "    PLEV    TMP[degC] spfh[kg/kg]  Tlcl[degC]  EQPT[degC]"
!     do k=ihgt,1,-1
!        write(6,"(F9.1,4F12.6)") &
!!             plev(k), data1(1,1,k)-273.15, data2(1,1,k)*1000, &
!!             tlcl(1,1,k)-273.15, eqpt(1,1,k)-273.15
!             plev(k), data1(1,1,k), data2(1,1,k)*1000, &
!             tlcl(1,1,k), eqpt(1,1,k)
!     enddo


!  !## get namelist
!  read(1,nampar)
!  write(6,nampar)
!  read(1,infile)
!  write(6,infile)
!  read(1,outfile)
!  write(6,outfile)

!  namelist/nampar/ ilon,ilat,ihgt,itime,undef
!  namelist/infile/  input1,input2,input3
!  namelist/outfile/  output1,output2
