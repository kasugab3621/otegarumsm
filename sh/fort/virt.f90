!! create virtual temperature
!!  2018.04.18 ks 
program virtual_temperature
  implicit none
  integer :: ilon,ilat,ihgt,itime
  real :: undef,x
  real,allocatable :: tmp(:,:,:),spfh(:,:,:)
  real,allocatable :: vt(:,:,:)
  character(150) :: path,var_tmp,var_spfh,var_vt
  integer :: i,j,k,it,isz
  
  read(5,*)itime
  read(5,*)ilon,ilat,ihgt
  allocate( &
       tmp(ilon,ilat,ihgt), &
       spfh(ilon,ilat,ihgt),&
       vt(ilon,ilat,ihgt))
       
  read(5,*)undef
  read(5,*)path
  read(5,*)var_tmp,var_spfh
  read(5,*)var_vt
  
  !## open
  open(1,file=trim(path)//"/"//trim(var_tmp),status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*ihgt*4)
  open(2,file=trim(path)//"/"//trim(var_spfh),status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*ihgt*4)
  open(10,file=trim(path)//"/"//trim(var_vt),status="replace",&
       access="direct",form="unformatted",recl=ilon*ilat*ihgt*4)
  
  do it=1,itime
     write(6,*) "now calculating ", it, "/", itime
     read(1,rec=it) tmp   !! temperature
     read(2,rec=it) spfh  !! specific humidity -> mixing ratio

     do k=1,ihgt
        do j=1,ilat
           do i=1,ilon
              if(tmp(i,j,k).ne.undef)then

                 if(spfh(i,j,k).ne.undef)then
                    ! spfh -> mixing ratio (x)
                    x=spfh(i,j,k)/(1-spfh(i,j,k))                    
                    ! virtual temperature 
                    vt(i,j,k)=tmp(i,j,k)*(1+0.61*x)
                 else
                    vt(i,j,k)=tmp(i,j,k)
                 end if
                    
              else
                 vt(i,j,k)=undef
              end if
              
           end do
        end do
     end do
     
     write(10,rec=it) vt

  end do
  
end program virtual_temperature

