program test2
  implicit none
  integer :: i,j,k,t,flag1
  integer,parameter :: ilon=241,ilat=253,ilev=21,itime=5
  real :: eqpt(ilon,ilat,ilev)
  real :: suf(ilon,ilat),mul(ilon,ilat)
  real :: maxe(ilon,ilat),undef
  undef=9.999e20
  
  open(10,file="/home/kasuga/otegaru-msm/testtesttest/data/jmbsc_msm/gradsbin/EQPTz.bin"&
       ,access="direct",status="old",recl=ilon*ilat*ilev*4)
  open(21,file="eqpt-suf-z.bin"&
       ,access="direct",status="unknown",recl=ilon*ilat*4)
  open(22,file="eqpt-mul-z.bin"&
       ,access="direct",status="unknown",recl=ilon*ilat*4)
  open(23,file="eqpt-mul-v.bin"&
       ,access="direct",status="unknown",recl=ilon*ilat*4)
  
  do t=1,itime
     read(10,rec=t) eqpt
     
     maxe(:,:)=0.
     
     do j=1,ilat
        do i=1,ilon
           
           flag1=0
           do k=1,6 !2500m
              
              if(eqpt(i,j,k).ne.undef)then
                 
                 if(flag1.eq.0)then
                    flag1=1
                    suf(i,j)=real(k)
                 end if
                 
                 if(eqpt(i,j,k).ge.maxe(i,j))then
                    maxe(i,j)=eqpt(i,j,k)
                    mul(i,j)=real(k)
                 end if

              else
                 suf(i,j)=undef
                 mul(i,j)=undef
                 maxe(i,j)=undef
              end if
              
           end do
        end do
     end do

     write(21,rec=t) suf
     write(22,rec=t) mul
     write(23,rec=t) maxe
     
     
  end do
  
  stop
end program test2
