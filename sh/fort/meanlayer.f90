!! create mean layer tmp & mixing-ratio(->spfh) for MLCAPE
!! note, output is 2-D data
!!  2018.04.18 ks 
program meanlayer
  implicit none
  integer :: ilon,ilat,ihgt,itime,flag
  real :: undef,mlmix,mlpot
  real,allocatable :: tmp(:,:,:),spfh(:,:,:),mix(:,:,:),&
       pres(:,:,:),pot(:,:,:)
  real,allocatable :: mlspfh(:,:),mlpres(:,:),mltmp(:,:)
  character(150) :: path,var_tmp,var_spfh,var_pres,&
       var_mlspfh,var_mlpres,var_mltmp
  integer :: i,j,k,it
  real :: rd,cp,kk
  rd=287 ; cp=1004 ; kk=rd/cp
    
  read(5,*)itime
  read(5,*)ilon,ilat,ihgt
  allocate( &
       tmp(ilon,ilat,ihgt), &
       spfh(ilon,ilat,ihgt),&
       pres(ilon,ilat,ihgt),&
       mix(ilon,ilat,ihgt),&
       pot(ilon,ilat,ihgt),&
       mltmp(ilon,ilat),&
       mlspfh(ilon,ilat),&
       mlpres(ilon,ilat)&
       )
  read(5,*)undef
  read(5,*)path
  read(5,*)var_tmp,var_spfh,var_pres
  read(5,*)var_mltmp,var_mlspfh,var_mlpres
  
  !## open
  open(1,file=trim(path)//"/"//trim(var_tmp),status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*ihgt*4)
  open(2,file=trim(path)//"/"//trim(var_spfh),status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*ihgt*4)
  open(3,file=trim(path)//"/"//trim(var_pres),status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*ihgt*4)
  open(11,file=trim(path)//"/"//trim(var_mltmp),status="unknown",&
       access="direct",form="unformatted",recl=ilon*ilat*4)
  open(13,file=trim(path)//"/"//trim(var_mlspfh),status="unknown",&
       access="direct",form="unformatted",recl=ilon*ilat*4)
  open(14,file=trim(path)//"/"//trim(var_mlpres),status="unknown",&
       access="direct",form="unformatted",recl=ilon*ilat*4)
  
  do it=1,itime
     write(6,*) "now calculating ", it, "/", itime
     read(1,rec=it) tmp   !! temperature
     read(2,rec=it) spfh  !! specific humidity -> mixing ratio
     read(3,rec=it) pres  !! pres at z
     
     do j=1,ilat
        do i=1,ilon
           
           flag=0
           do k=1,3 !under 1000m              
              
              if(spfh(i,j,k).eq.undef.or.tmp(i,j,k).eq.undef&
                   .or.pres(i,j,k).eq.undef)then
                 flag=1
              else
                 ! spfh -> mixing ratio (x)
                 mix(i,j,k)=spfh(i,j,k)/(1-spfh(i,j,k))
                 ! potential temperature
                 pot(i,j,k)=tmp(i,j,k)*((1000/pres(i,j,k))**kk)
              end if
              
           end do !k

           if(flag.eq.0)then
              
              ! mean under 1000m              
              !! log fitting for pres vs hgt
              mlpres(i,j)=log(pres(i,j,1))+log(pres(i,j,2))+log(pres(i,j,3))
              mlpres(i,j)=exp(mlpres(i,j)/3)
              
              !! mix -> spfh
              mlmix=mix(i,j,1)+mix(i,j,2)+mix(i,j,3)
              mlmix=mlmix/3
              mlspfh(i,j)=mlmix/(1+mlmix)
              
              !! pot -> tmp -> virtual tmp with mlmix & mlpres
              mlpot=pot(i,j,1)+pot(i,j,2)+pot(i,j,3)
              mlpot=mlpot/3
              mltmp(i,j)=mlpot/((1000/mlpres(i,j))**kk)
            
           else
              
              mltmp(i,j)=undef
              mlspfh(i,j)=undef
              mlpres(i,j)=undef
              
           end if
           
        end do
     end do

     write(11,rec=it) mltmp
     write(13,rec=it) mlspfh
     write(14,rec=it) mlpres
     
  end do
  
end program meanlayer

