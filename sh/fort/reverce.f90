program reverce
  implicit none
  integer :: i,j,k,l,t
  integer,parameter :: ilat=505,ilon=481
  real :: topo(ilon,ilat),rtopo(ilon,ilat)

  open(10,file="/home/kasuga/otegaru-msm/sh/TOPO.MSM_5K_20071121",&
       access="direct",form="unformatted",status="old",&
       recl=ilon*ilat*4)
  open(20,file="/home/kasuga/otegaru-msm/sh/rtopo",&
       access="direct",form="unformatted",status="replace",&
       recl=ilon*ilat*4)
  
  read(10,rec=1) topo
  
  do j=1,ilat
     rtopo(:,j)=topo(:,ilat-j+1)     
  end do
  
  write(20,rec=1) rtopo
  stop
end program reverce

