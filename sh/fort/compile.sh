#!/bin/bash

#fort_compiler="ifort -convert big_endian -assume byterecl -heap-arrays"
#fort_compiler="gfortran -fconvert=big-endian -frecord-marker=4"
fort_compiler="f95 -fconvert=big-endian"

$fort_compiler eqptp.f90 -o eqptp.exe
$fort_compiler rh2shp2.f90 -o rh2shp2.exe

$fort_compiler srh.f90 -o srh.exe

$fort_compiler virt.f90 -o virt.exe
$fort_compiler p2z-m10m.f90 -o p2z-m10m.exe
$fort_compiler rh2shz.f90 -o rh2shz.exe
$fort_compiler eqptz-s.f90 -o eqptz-s.exe
$fort_compiler meanlayer.f90 -o meanlayer.exe
$fort_compiler liftlayers2.f90 -o liftlayers.exe
$fort_compiler capev2.f90 -o capev2.exe

