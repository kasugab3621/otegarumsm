!! grid conversion (ln(p) -> z)  with undefinization
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!! NEED
!!  1.hight data (isobar)
!!  2.slp data (surface)
!!  3.sfcp data (surface)
!! EDIT in-file(isobar,surface)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!   2016.02.?? kasuga satoru
!!        10.07 undef chousei by kasuga 

program p2z
  implicit none
  integer :: i,j,si,sj,k,l,t
  integer :: rj
  integer :: ilat,ilon,slat,slon,ip,iz,itime,sp,ep,ez
  integer :: sflag,eflag,lflag
  real,allocatable :: ppres(:),zhgt(:),zpres(:,:,:),phgt(:,:,:)
  real,allocatable :: pin(:,:,:),zout(:,:,:),sfcp(:,:),sfcin(:,:)
  real,allocatable :: topo(:,:)
  real :: tan,ptan,dif1,dif2,difmin,undef,tpundef
  character(150) :: flat,path,name_hgt,name_pin,&
       name_zout,name_sfcp,name_pout,name_sfcin,&
       name_topo
  tpundef=-999.9
  
  read(5,*)itime
  read(5,*)ilon,ilat,slon,slat,ip,iz

  allocate(phgt(ilon,ilat,ip),pin(ilon,ilat,ip),&
       ppres(ip),zhgt(iz),sfcin(slon,slat),&
       zpres(ilon,ilat,iz),zout(ilon,ilat,iz),&
       sfcp(slon,slat),topo(slon,slat))
    
  read(5,*)ppres
  read(5,*)zhgt
  read(5,*)undef
  read(5,*)path
  read(5,*)name_topo
  read(5,*)name_hgt,name_sfcp
  read(5,*)name_pin,name_sfcin
  read(5,*)name_zout,name_pout
  
  open(1,file=trim(path)//"/"//trim(name_hgt),status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*ip*4)
  open(2,file=trim(path)//"/"//trim(name_pin),status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*ip*4)
  open(11,file=trim(path)//"/"//trim(name_zout),status="replace",&
       access="direct",form="unformatted",recl=ilon*ilat*iz*4)
  open(12,file=trim(path)//"/"//trim(name_pout),status="replace",&
       access="direct",form="unformatted",recl=ilon*ilat*iz*4)
  open(50,file=trim(path)//"/"//trim(name_sfcp),status="old",&
       access="direct",form="unformatted",recl=slon*slat*4)
  open(51,file=trim(path)//"/"//trim(name_sfcin),status="old",&
       access="direct",form="unformatted",recl=slon*slat*4)
  open(99,file=trim(name_topo),status="old",&
       access="direct",form="unformatted",recl=slon*slat*4)

  read(99,rec=1) topo

  do t=1,itime
     print *, "now calculating ",t,"/",itime
     
     read(1,rec=t) phgt
     read(2,rec=t) pin
     read(50,rec=t) sfcp
     read(51,rec=t) sfcin

     ! inititalize     
     zpres(:,:,:)=undef
     zout(:,:,:)=undef
     
     do j=1,ilat
        sj=2*j-1
        do i=1,ilon
           si=2*i-1
                  
           ! search loop's start & goal point
           sp=1    ; ep=ip   ; ez=iz
           sflag=0 ; eflag=0 ; lflag=0
           do k=1,ip
              if(ppres(k).le.sfcp(si,sj)/100.and.sflag.eq.0) then
                 sp=k
                 sflag=1
              end if
              
              if(phgt(i,j,k).ne.undef.and.&
                   phgt(i,j,k).ge.zhgt(iz).and.eflag.eq.0) then
!                 print *, phgt(i,j,k),zhgt(iz)
                 ep=k
                 eflag=1
              end if
              
           end do           
           
           do l=1,iz                 
              ! when your reqested z-top-level(m) is too tall                 
              if(zhgt(l).ge.phgt(i,j,ip).and.lflag.eq.0.and.&
                   eflag.eq.0) then
                 ez=l-1
                 lflag=1                    
              end if
           end do

           !! sea and plain (<200m) using surf. data
           if(topo(si,sj).ne.tpundef.and.topo(si,sj).le.200) then
              zout(i,j,1)=sfcin(si,sj)
              zpres(i,j,1)=sfcp(si,sj)/100
           end if
              
           !! main loop
           do l=2,ez

              do k=sp,ep

                 ! find nearest pres surface for interval zhgt
                 if((phgt(i,j,k)-zhgt(l))*(phgt(i,j,k+1)-zhgt(l))&
                      .lt.0.) then
                    
                    zpres(i,j,l)=exp((log(ppres(k+1))*(zhgt(l)-phgt(i,j,k))&
                         +log(ppres(k))*(phgt(i,j,k+1)-zhgt(l)))&
                         /(phgt(i,j,k+1)-phgt(i,j,k)))
                    
                    if(pin(i,j,k).ne.undef.and.&
                         pin(i,j,k+1).ne.undef.and.&
                         zpres(i,j,l).ne.undef) then
                       zout(i,j,l)=(pin(i,j,k)*(ppres(k+1)-zpres(i,j,l))&
                            -pin(i,j,k+1)*(ppres(k)-zpres(i,j,l)))&
                            /(ppres(k+1)-ppres(k))
                    end if
                    
                 end if
                 
              end do !k
              
           end do !l
           

        end do !i              
     end do !j

     write(11,rec=t) zout
     write(12,rec=t) zpres
     
  end do ! tloop
  
  stop
end program p2z



!           do l=1,iz
 !             print *, zpres(1,1,l)
 !          end do

           


!print *, ppres(iml)-ptan*(zhgt(l)-phgt(i,j,iml))
!print *, zpres(i,j,l), sfcp(si,sj)/100.
!print *, si,sj,sfcp(si,sj)



!if(phgt(i,j,k).eq.undef.or.in(i,j,k).eq.undef) then
!   out(i,j,k)=undef
!   zpres(i,j,k)=undef
!end if




 !        read(5,*)flat
 ! open(10,file=flat)
 ! do j = 1, ilat
 !    read(10,*) lat(j)
 ! enddo



!dif1=abs(zhgt(l)-phgt(i,j,k))                
!if(dif1.le.difmin) then
!   dif2=difmin ! sakkino champion
!   difmin=dif1 ! shin champion
!   im2=im1
!   im1=k
!end if

!if(im1.lt.im2) then
!   imu=im2  ! upper
!   iml=im1  ! lower
!else
!   imu=im1
!   iml=im2
!end if



!if(phgt(i,j,iml).eq.undef) then
!   zout(i,j,l)=undef
!   zpres(i,j,l)=undef
!else
   

! interpolate
!   tan=(pin(i,j,imu)-pin(i,j,iml))/(phgt(i,j,imu)-phgt(i,j,iml))
!   
!   zpres(i,j,l)=ppres(iml)-ptan*(zhgt(l)-phgt(i,j,iml))
!      
!   if(sfcp(si,sj)/100..lt.zpres(i,j,l)) then
!      zout(i,j,l)=undef
!      zpres(i,j,l)=undef
!      !                print *, "undef!!!"
!   else
!      zout(i,j,l)=pin(i,j,iml)+tan*(zhgt(l)-phgt(i,j,iml))
!   end if
!   
!endif
