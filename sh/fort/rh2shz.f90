! specific humidity using tetens (AMS Glossary)
!  for z-coodinate data
!  interpolate? upper level where rh is undef
!! ########################
!  indata   : RH,TMP,PRES
!  outdata  : SH,SSH,
!! ########################
!        2016,10,17 kasuga 
program rh2sh
  real,allocatable :: rh(:,:,:),tmp(:,:,:),sh(:,:,:),&
       ev(:,:,:),es(:,:,:),pres(:,:,:),ex(:,:,:),&
       ssh(:,:,:),plev(:)
  character(150) :: flat,path,var_rh,var_tmp,var_pres,&
       var_sh,var_ssh
  real :: co, cel
  real,parameter :: l=2.5e6, rv=461., srh=100.

  co=l/rv
  read(5,*)itime
  read(5,*)ilon,ilat,ilev
  
  allocate(rh(ilon,ilat,ilev),tmp(ilon,ilat,ilev),sh(ilon,ilat,ilev),&
       ssh(ilon,ilat,ilev),ev(ilon,ilat,ilev),es(ilon,ilat,ilev),&
       pres(ilon,ilat,ilev),plev(ilev),ex(ilon,ilat,ilev))
  !read(5,*)plev
  read(5,*)undef
  read(5,*)path
  read(5,*)var_rh,var_tmp,var_pres
  read(5,*)var_sh,var_ssh
  
     
     open(1,file=trim(path)//"/"//trim(var_rh),status="old",&
          access="direct",form="unformatted",recl=ilon*ilat*ilev*4)
     open(2,file=trim(path)//"/"//trim(var_tmp),status="old",&
          access="direct",form="unformatted",recl=ilon*ilat*ilev*4)
     open(3,file=trim(path)//"/"//trim(var_pres),status="old",&
          access="direct",form="unformatted",recl=ilon*ilat*ilev*4)
     open(11,file=trim(path)//"/"//trim(var_sh),status="unknown",&
          access="direct",form="unformatted",recl=ilon*ilat*ilev*4)
     open(12,file=trim(path)//"/"//trim(var_ssh),status="unknown",&
          access="direct",form="unformatted",recl=ilon*ilat*ilev*4)
     
     
     do it=1,itime
        print *, "now calculating ",it,"/",itime
  
     read(1,rec=it) rh
     read(2,rec=it) tmp
     read(3,rec=it) pres

     
     do k=1,ilev
        do j=1,ilat
           do i=1,ilon

              if(tmp(i,j,k).eq.undef.or.pres(i,j,k).eq.undef.or.&
                   rh(i,j,k).eq.undef) then
                 sh(i,j,k)=undef
                 ssh(i,j,k)=undef
              else
                 cel=tmp(i,j,k)-273.15                                  
                 es(i,j,k)=10*0.611*10**(7.5*cel/(cel+237.3)) !tetens (AMS Grossary)
                 ev(i,j,k)=rh(i,j,k)*es(i,j,k)/100  !vapore perssure[hPa]
                 
                 sh(i,j,k)=0.622*ev(i,j,k)/(pres(i,j,k)-0.378*ev(i,j,k))
                 ssh(i,j,k)=0.622*es(i,j,k)/(pres(i,j,k)-0.378*es(i,j,k))
                 
              end if
             
           end do
        end do
     end do

     write(11,rec=it) sh
     write(12,rec=it) ssh
     

  end do
  stop
end program rh2sh

! previous ver.
!ex(i,j,k)=exp(co*(tmp(i,j,k)-273.2)/(tmp(i,j,k)*273.2))
!ev(i,j,k)=rh(i,j,k)*6.11*ex(i,j,k)/100  !vapore perssure[hPa]

!bad? tetens es(i,j,k) = 6.11*exp(17.27*(tmp(i,j,k)-273.15)/(tmp(i,j,k)-35.86))
