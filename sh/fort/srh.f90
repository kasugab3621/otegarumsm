program sreh
  implicit none
  integer :: i,j,k,l,t,flag
  integer :: ilon,ilat,iz,uz,lz,itime
  real,allocatable :: u(:,:,:),v(:,:,:),&
       uc(:,:),vc(:,:),srh1(:,:),&
       um(:,:),vm(:,:),srh3(:,:),&
       srhpo(:,:,:),sfc(:,:)!,srh31(:,:)
  real :: ul,uu,vl,vu!,um,vm
  real :: tan,stan,undef,dd,dif
  character(150) :: path,name_u,name_v,&
       name_srhpo,name_allsrh
       !name_srh1,name_srh3,name_srh31

  read(5,*)itime
  read(5,*)ilon,ilat,iz

  allocate(u(ilon,ilat,iz),v(ilon,ilat,iz),&
       uc(ilon,ilat),vc(ilon,ilat),srh1(ilon,ilat),&
       um(ilon,ilat),vm(ilon,ilat),srh3(ilon,ilat),&
       srhpo(ilon,ilat,iz),sfc(ilon,ilat))!,srh31(ilon,ilat))
  

  read(5,*)undef
  read(5,*)path
  read(5,*)name_u,name_v  
  read(5,*)name_allsrh,name_srhpo
  !read(5,*)name_srh3,name_srh1,name_srh31
  
  open(1,file=trim(path)//"/"//trim(name_u),status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*iz*4)
  open(2,file=trim(path)//"/"//trim(name_v),status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*iz*4)
  open(11,file=trim(path)//"/"//trim(name_allsrh),status="unknown",&
       access="direct",form="unformatted",&
       recl=ilon*ilat*7*4)
  !open(12,file=trim(path)//"/"//trim(name_srhpo),status="unknown",&
  !     access="direct",form="unformatted",&
  !     recl=ilon*ilat*iz*4)
  !open(20,file=trim(path)//"/sfc.grd",status="unknown",&
  !     access="direct",form="unformatted",&
  !     recl=ilon*ilat*4)  
  !open(13,file=trim(path)//"/"//trim(name_srh3),status="unknown",&
  !     access="direct",form="unformatted",&
  !     recl=ilon*ilat*4)
  !open(14,file=trim(path)//"/"//trim(name_srh1),status="unknown",&
  !     access="direct",form="unformatted",&
  !     recl=ilon*ilat*4)
  !open(15,file=trim(path)//"/"//trim(name_srh31),status="unknown",&
  !     access="direct",form="unformatted",&
  !     recl=ilon*ilat*4)

  print *, 'start'
  
  do t=1,itime
!  do t=1,1
     read(1,rec=t) u
     read(2,rec=t) v
     print *, "now calculating srh.f90", t, "/", itime
     do j=1,ilat
        do i=1,ilon
!     do j=577,577
!        do i=515,515

!!! srtom motion (Bunkers etal., 2000)
           ! initial parm
           flag=0
           do k=1,13 ! under 6000m

              !print *, u(i,j,k)              
              
              !if(u(i,j,k).lt.200.and.u(i,j,k).gt.-200.and.flag.eq.0) then
              if(u(i,j,k).ne.undef.and.u(i,j,k+1).ne.undef.and.flag.eq.0) then
                 l=k
                 flag=1
                 exit
              end if
           end do

           if(flag.eq.0) then
              print *, 'flag 0'
              srh3(i,j)=undef;srh1(i,j)=undef
              uc(i,j)=undef;vc(i,j)=undef
              um(i,j)=undef;vm(i,j)=undef              
              srhpo(i,j,:)=undef
              srh3(i,j)=undef;srh1(i,j)=undef
              exit
           end if

           sfc(i,j)=real(l)
           
!1   2    3    4    5    6    7    8    9   10   11   12   13              
!0,500,1000,1500,2000,2500,3000,3500,4000,4500,5000,5500,6000,
           ul=(u(i,j,l)+u(i,j,l+1))/2
           uu=(u(i,j,l+11)+u(i,j,l+12))/2
           vl=(v(i,j,l)+v(i,j,l+1))/2
           vu=(v(i,j,l+11)+v(i,j,l+12))/2

           !print *,u(i,j,l),u(i,j,l+1),l
           
           um(i,j)=0.; vm(i,j)=0
           do k=l,l+12
              um(i,j)=um(i,j)+u(i,j,k)
              vm(i,j)=vm(i,j)+v(i,j,k)
           end do
           um(i,j)=um(i,j)/13
           vm(i,j)=vm(i,j)/13
           ! calc cross point
           dif=7.5

           
          if(uu.ne.ul) then
              tan=(vu-vl)/(uu-ul)
           end if

           ! calc storm motion
           if(tan.ne.0.) then
              stan=(tan**2+1)**(-0.5)
              dd=abs(stan*tan*dif)
              if(vu.gt.vl) then
                 uc(i,j)=um(i,j)+dd
                 vc(i,j)=vm(i,j)-dd/tan
                 
              else if(vu.lt.vl) then
                 uc(i,j)=um(i,j)-dd
                 vc(i,j)=vm(i,j)+dd/tan
              end if
           else
              dd=0
              if(uu.gt.ul) then
                 uc(i,j)=um(i,j)
                 vc(i,j)=vm(i,j)-7.5
              else if(uu.lt.ul) then
                 uc(i,j)=um(i,j)
                 vc(i,j)=vm(i,j)+7.5
              else
                 uc(i,j)=um(i,j)
                 vc(i,j)=vm(i,j)                 
              end if
           end if

           srh1(i,j)=0.
           srh3(i,j)=0.

!1   2    3    4    5    6    7    8    9   10   11   12   13              
!0,500,1000,1500,2000,2500,3000,3500,4000,4500,5000,5500,6000,6500,7000,7500,8000,8500,9000,9500,10000
           !print *,l+6,iz

           
           if(l+6.le.iz) then
              do k=l,l+6
                 srh3(i,j)=srh3(i,j)-(u(i,j,k)-uc(i,j))*(v(i,j,k+1)-v(i,j,k))&
                      +(v(i,j,l)-vc(i,j))*(u(i,j,k+1)-u(i,j,k))
              end do
              do k=l,l+2
                 srh1(i,j)=srh1(i,j)-(u(i,j,k)-uc(i,j))*(v(i,j,k+1)-v(i,j,k))&
                      +(v(i,j,l)-vc(i,j))*(u(i,j,k+1)-u(i,j,k))
              end do
           else
              srh3(i,j)=undef
              srh1(i,j)=undef
              !print *, 'l+6 iz'
           endif
           
           
           if (srh3(i,j).eq.undef) then
              print *,i,j,l+6
              stop
           endif
           
           !if (srh3(i,j).lt.-500000.) then
           !   do k=1,iz
           !      print *, u(i,j,k),k
           !   end do
           !   stop
           !   
           !endif
           
!           do k=1,iz-1
!              if(u(i,j,k).eq.undef) then
!                 srhpo(i,j,k)=undef
!                 !print *,"hgoe in1"
!              else
!                 srhpo(i,j,k)=-(u(i,j,k)-uc(i,j))*(v(i,j,k+1)-v(i,j,k))&
!                      +(v(i,j,l)-vc(i,j))*(u(i,j,k+1)-u(i,j,k))
!                 !print *,"hgoe in2"
!                 !print *, "-----------------"
!                 !print *, k
!                 !print *, uc(i,j),vc(i,j)
!                 !print *, u(i,j,k),v(i,j,k)
!                 !print *, srhpo(i,j,k)
!              end if
!              !print *,"hoge in3"
!           end do

!           srhpo(i,j,iz)=undef

        end do !lon
     end do !lat
     write(11,rec=t) srh3,srh1,uc,vc,um,vm,sfc
!     write(20,rec=t) sfc
!     write(12,rec=t) srhpo
     !write(13,rec=t) srh3
     !write(14,rec=t) srh1

     !do j=1,ilat
     !   do i=1,ilon
     !      if(srh3(i,j).ne.undef) then
     !         srh31(i,j)=srh3(i,j)+srh1(i,j)/2
     !      else
     !         srh31(i,j)=undef
     !      endif
     !   end do
     !end do

     !write(15,rec=t) srh31
     
  end do
  
  stop
end program sreh

 !             ftan=1./(tan**2+1)
 !             dd1=ftan(tan*(vm(i,j)-vl)+um(i,j)-ul)
 !             un=ul+dd1
 !             vn=vl+dd1*tan
!           else
!              dd1=0
!              un=uu
!              vn=vm(i,j)
!           end if



     
!  open(1,file="~/Work8_20150415_CV/data/MSM/14-16/UGRDz_u.bin",access="direct",&
!       status="old",rec=ilon*ilat*ilev*4)
!  open(2,file="~/Work8_20150415_CV/data/MSM/14-16/VGRDz_u.bin",access="direct",&
!       status="old",rec=ilon*ilat*ilev*4)
!  open(11,file="~/Work8_20150415_CV/data/MSM/14-16/SRH.bin",access="direct",&
!       status="unknown",rec=ilon*ilat*4)



!           lk=k; uk=k+1
!              srh(i,j)=srh(i,j)-(u(i,j,lk)-uc(i,j))*(v(i,j,uk)-v(i,j,lk))&
!                   +(v(i,j,lk)-vc(i,j))*(u(i,j,uk)-u(i,j,lk))
!              print *, "sreh1",srh(i,j)

!              print *, "ru",u(i,j,k)-uc(i,j)
!              print *, "sv",v(i,j,k+1)-v(i,j,k)
!              print *, "rv",v(i,j,l)-vc(i,j)
!              print *, "su",u(i,j,k+1)-u(i,j,k)
!              print *, "sreh2",-(u(i,j,k)-uc(i,j))*(v(i,j,k+1)-v(i,j,k))&
!                   +(v(i,j,l)-vc(i,j))*(u(i,j,k+1)-u(i,j,k))
!              print *, "sreh3",srh(i,j)
!              print *, "****************"

