#!/bin/bash

cmf="f95 -fconvert=big-endian"

# p2z-m10m.f90 rh2shz.f90 eqptz-s.f90 
# meanlayer.f90 liftlayers2.f90 capev2.f90

$cmf p2z-m10m.f90 -o p2z-m10m.exe
$cmf rh2shz.f90 -o rh2shz.exe
$cmf eqptz-s.f90 -o eqptz-s.exe
$cmf meanlayer.f90 -o meanlayer.exe
$cmf liftlayers2.f90 -o liftlayers.exe
$cmf capev2.f90 -o capev2.exe


