#!/bin/sh

## path to data dir 
MSMBIN="."  ;#current dir
## time duration (JST) 
DATEST1="2018/09/04 00:00"
DATEED1="2018/09/05 12:00"

## area of analysis (default 22.4/47.6/120/150)
#LATSI=22.4;LATEI=47.6;LONSI=120;LONEI=150   ;#default msm
LATSI=34;LATEI=45;LONSI=130;LONEI=144;

## MSM

TOPO=$SHDIR/rtopo

## SET_DATE *convert JST -> UTC
DATESTI=`date -d "$DATEST1 9 hour ago" +"%Y/%m/%d %H:%M"`
DATEEDI=`date -d "$DATEED1 9 hour ago" +"%Y/%m/%d %H:%M"`
DATEKEI=`date -d "$DATEKEY 9 hour ago" +"%Y/%m/%d %H:%M"`

DATEST=`date -d "$DATESTI" +%Y%m%d%H%M`
    SS=`date -d "$DATESTI" +%s`
YYS=`echo $DATEST | cut -c 1-4`
MMS=`echo $DATEST | cut -c 5-6`
DDS=`echo $DATEST | cut -c 7-8`
HHS=`echo $DATEST | cut -c 9-10`
NNS=`echo $DATEST | cut -c 11-12`
DATEED=`date -d "$DATEEDI" +%Y%m%d%H%M`
    ES=`date -d "$DATEEDI" +%s`
YYE=`echo $DATEED | cut -c 1-4`
MME=`echo $DATEED | cut -c 5-6`
DDE=`echo $DATEED | cut -c 7-8`
HHE=`echo $DATEED | cut -c 9-10`
NNE=`echo $DATEED | cut -c 11-12`
DATEK=`date -d "$DATEKEI" +%H"Z"%d%b%Y`

diff=`expr $ES - $SS`
min=`expr $diff / 600`
FNUM10m=`expr $min + 1`
hour=`expr $diff / 3600 / 3`
FNUM3h=`expr $hour + 1`

#~set -evx
#set -e

