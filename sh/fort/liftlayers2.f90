!!===================================================================
!! calc. cape_cin 
!!  first calculate (bad lifted tmp. (to2))
!! ***********************
!! ** NOTE, USE Z-COODINATE DATA
!! ***********************
!!   2016.03.27 by kasuga satoru
!!        10.18 good undef
!!   2018.02.26 not need prepare "s"spfh
!!   2018.04.18 1. add CAPE relative parameters (subspecies)
!!     SBCAPE(2m), CAPE500m, MUCAPE(<2500m), MLCAPE(0~1000m)
!!===================================================================
program main
  implicit none
  integer :: ilon,ilat,ihgt,itime,flag,muflag
  real :: undef
  real :: maxe,eqpto,lcl
  real,allocatable :: tmp(:,:,:),pres(:,:,:)
  real,allocatable :: eqpt(:,:,:),seqpt(:,:,:)
  real,allocatable :: qvs(:,:,:),es(:,:,:)
  real,allocatable :: zlev(:),zlcl(:,:,:)
  real,allocatable :: eqptml(:,:),lclml(:,:)
  real,allocatable :: seqptz(:),presz(:),tmpz(:),to2z(:)
  real,allocatable :: to2sb(:,:,:),to25(:,:,:),to2mu(:,:,:),to2ml(:,:,:)
  integer :: szsb,sz5,szmu,szml
  character(150) :: path
  character(150) :: var_tmp,var_eqpt,var_seqpt,var_pres,var_zlcl
  character(150) :: var_eqptml,var_lclml
  character(150) :: var_to2sb,var_to25,var_to2mu,var_to2ml
  integer :: i,j,k,it
  
  !## read in-file
  read(5,*)itime
  read(5,*)ilon,ilat,ihgt
  allocate( &
       tmp(ilon,ilat,ihgt),pres(ilon,ilat,ihgt),&       
       qvs(ilon,ilat,ihgt),es(ilon,ilat,ihgt),&
       eqpt(ilon,ilat,ihgt),seqpt(ilon,ilat,ihgt),&
       eqptml(ilon,ilat),lclml(ilon,ilat),&
       zlcl(ilon,ilat,ihgt),zlev(ihgt),&
       seqptz(ihgt),presz(ihgt),tmpz(ihgt),to2z(ihgt),&
       to2sb(ilon,ilat,ihgt),to25(ilon,ilat,ihgt),&
       to2mu(ilon,ilat,ihgt),to2ml(ilon,ilat,ihgt)&
       )
  read(5,*)zlev
  read(5,*)undef
  read(5,*)path
  read(5,*)var_tmp,var_eqpt,var_seqpt,var_pres,var_zlcl
  read(5,*)var_eqptml,var_lclml
  read(5,*)var_to2sb,var_to25,var_to2mu,var_to2ml
  
  
  !## open
  open(1,file=trim(path)//"/"//trim(var_tmp),status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*ihgt*4)
  open(2,file=trim(path)//"/"//trim(var_eqpt),status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*ihgt*4)
  open(3,file=trim(path)//"/"//trim(var_seqpt),status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*ihgt*4)
  open(4,file=trim(path)//"/"//trim(var_pres),status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*ihgt*4)
  open(7,file=trim(path)//"/"//trim(var_zlcl),status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*ihgt*4)
  
  open(8,file=trim(path)//"/"//trim(var_eqptml),status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*4)
  open(9,file=trim(path)//"/"//trim(var_lclml),status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*4)
  
  open(11,file=trim(path)//"/"//trim(var_to2sb),status="replace",&
       access="direct",form="unformatted",recl=ilon*ilat*ihgt*4)
  open(12,file=trim(path)//"/"//trim(var_to25),status="replace",&
       access="direct",form="unformatted",recl=ilon*ilat*ihgt*4)
  open(13,file=trim(path)//"/"//trim(var_to2mu),status="replace",&
       access="direct",form="unformatted",recl=ilon*ilat*ihgt*4)
  open(14,file=trim(path)//"/"//trim(var_to2ml),status="replace",&
       access="direct",form="unformatted",recl=ilon*ilat*ihgt*4)
  
  
  !## start calc loop
  !###########################################################
  do it=1,itime
     print *, "now calculating ", it, "/", itime
     read(1,rec=it) tmp    !! temperature (actual or virtual)
     read(2,rec=it) eqpt   !! equivalent pot.
     read(3,rec=it) seqpt  !! saturated eqpt.
     read(4,rec=it) pres   !! press at isohight
     read(7,rec=it) zlcl   !! zlcl ,3D
     read(8,rec=it) eqptml !! eqpt at mean level, 2D
     read(9,rec=it) lclml  !! zlcl at mean level, 2D
     
     !## first calculation
     !###########################################################
     !## calc lifted air temp. firstly (bad accuracy)

     do j=1,ilat
        do i=1,ilon

           szsb=0 ; sz5=0 ; szmu=0 ; szml=2

           !search lifting layer for each cape type
           flag=0
           do k=1,ihgt 
              !## szsb = lifted from surface
              !## sz5 = lifted from 5m AGL
              if(tmp(i,j,k).ne.undef.and.flag.eq.0)then                 
                 szsb=k
                 sz5=k+1
                 flag=1
              end if
           end do

           maxe=0. ; muflag=0
           do k=1,7
              !## szmu = lifted from most unstable layer under 3000m
              if(eqpt(i,j,k).ne.undef.and.eqpt(i,j,k).gt.maxe)then
                 maxe=eqpt(i,j,k)
                 szmu=k
              end if
           end do

           if(maxe.eq.0.)then
              muflag=1
           end if
           
           ! main loop
           !####################
           !do k=1,ihgt
           
           !if(tmp(i,j,k).ne.undef.and.pres(i,j,k).ne.undef.and.&
           !eqpt(i,j,k).ne.undef)then
           
           seqptz(:)=seqpt(i,j,:)
           presz(:)=pres(i,j,:)
           tmpz(:)=tmp(i,j,:)
           
           eqpto=eqpt(i,j,szsb)
           lcl=zlcl(i,j,szsb)
           call lift_tmp(ihgt,szsb,lcl,undef,eqpto,seqptz,zlev,presz,tmpz,to2z)
           to2sb(i,j,:)=to2z(:)
           
           eqpto=eqpt(i,j,szsb+1)
           lcl=zlcl(i,j,szsb+1)
           call lift_tmp(ihgt,szsb+1,lcl,undef,eqpto,seqptz,zlev,presz,tmpz,to2z)
           to25(i,j,:)=to2z(:)

           if(muflag.ne.1)then
              eqpto=eqpt(i,j,szmu)
              lcl=zlcl(i,j,szmu)
              call lift_tmp(ihgt,szmu,lcl,undef,eqpto,seqptz,zlev,presz,tmpz,to2z)
              to2mu(i,j,:)=to2z(:)
           else
              to2mu(i,j,:)=undef
           end if           

           if(eqptml(i,j).ne.undef)then
              eqpto=eqptml(i,j)
              lcl=lclml(i,j)
              call lift_tmp(ihgt,szml,lcl,undef,eqpto,seqptz,zlev,presz,tmpz,to2z)
              to2ml(i,j,:)=to2z(:)
           else
              to2ml(i,j,:)=undef
           end if
           
        end do !i
     end do !j
     
     write(11,rec=it) to2sb
     write(12,rec=it) to25
     write(13,rec=it) to2mu
     write(14,rec=it) to2ml
     
  end do

  stop
end program main

subroutine lift_tmp(ihgt,sz,lcl,undef,eqpto,seqptz,zlev,presz,tmpz,to2z)
  implicit none
  integer :: k
  integer,intent(in) :: ihgt,sz
  real,intent (in) :: lcl,undef,eqpto
  real,intent(in) :: zlev(ihgt),seqptz(ihgt)
  real,intent(in) :: presz(ihgt),tmpz(ihgt)
  real,intent(out) :: to2z(ihgt)
  real :: es,qvs,dptes,poto,pot,const,varii
  real, parameter :: Cpd = 1004.d0
  real, parameter :: Rd  = 287.d0
  real, parameter :: Lv  = 2.500E+6
  real, parameter :: eps = 0.622
  real, parameter :: dz = 500.d0
  real, parameter :: gamd = 9.8000E-3
  const = Lv**2 / (Cpd * Rd)  
  to2z(:)=undef
  
  do k=sz,ihgt
     
     !teten's formula
     es=6.11*exp(17.27*(tmpz(k)-273.15)/(tmpz(k)-35.86))
     qvs=eps*es/(presz(k)-es)
     !make const
     varii=(eps+qvs)*qvs/tmpz(k)**2
     pot=tmpz(k)*((1000./presz(k))**(Rd/Cpd))
     
     if(zlev(k).le.lcl) then
        to2z(k)=tmpz(sz)-(zlev(k)-zlev(sz))*gamd
     else
        if(seqptz(k).ne.undef)then
           !## calc diff lifted eqpt and ambient air saturated epqt
           dptes=eqpto-seqptz(k)
           !## calc lifted air temp. (bad accuracy)
           poto=pot+dptes/(1+const*varii)
           to2z(k)=poto/((1000./presz(k))**(Rd/Cpd))
           !print*,"hoge32",to2mu(i,j,k)
        else
           to2z(k)=undef
        end if
        
     end if

  end do
  
end subroutine lift_tmp
