!!===================================================================
!! calc. CAPE CIN etc. with Yoshizaki and Kato (2007) method (see A-3)
!!===================================================================
!!  calculate secondary lifted parcel's temperature with good accuracy
!!  using firstly lifted data from liftlayers.f90 
!! ***********************
!! ** NOTE! use z-coodinate data
!! ***********************
!!  CAPE : all positive buoyancy above LFC
!!  CIN  : negative buoyancy bet. sfc ~ LFC only
!!  LFC  : selected lower one
!!  LNB  : selected higher one (see subroutine)
!!  
!! ***********************
!!   2016.03.27 by kasuga satoru
!!        10.18 good undef
!!   2018.02.26 not need prepare "s"spfh
!!   2018.04.18 1. add CAPE-relatives (subspecies)
!!     SBCAPE(2m), CAPE500m, MUCAPE(<3000m), MLCAPE(0~1000m)
!!              2. update to use virtual tmp. along with actual tmp.
!!     
!!===================================================================
program main
  implicit none
  integer :: ilon,ilat,ihgt,itime,ilay
  integer :: flag,uflag,lnbflag,lfcflag,sz,ez,sl
  real :: undef,const,varii,qvs,es,es3,lfcp,lnbp
  real :: pot1,pot2,poto3,pot,dptes,mxr_a,mxr_o
  real,allocatable :: tmp2(:,:,:),tmp1(:,:,:),pres(:,:,:)
  real,allocatable :: eqpt(:,:,:),seqpt2(:,:,:),sh(:,:,:)
  real,allocatable :: zlev(:),zlcl(:,:,:),to3(:,:,:)
  real,allocatable :: tmp1v(:,:,:),to3v(:,:,:),vtm(:,:,:)
  real,allocatable :: lcl(:,:),eqpto(:,:),hgtos(:,:),presos(:,:)
  real,allocatable :: tdif(:),tdifv(:),pe(:),pev(:),dz(:)
  real,allocatable :: cape(:,:),cin(:,:),lnb(:,:),lfc(:,:)
  real,allocatable :: capev(:,:),cinv(:,:),lnbv(:,:),lfcv(:,:)
  character(150) :: path,var_tmp2,var_tmp1,var_eqpt,var_seqpt2
  character(150) :: var_sh,var_pres,var_zlcl
  character(150) :: var_cape,var_to3,var_capev,var_to3v
  integer :: i,j,k,it
  real,parameter :: g   = 9.807
  real,parameter :: Cpd = 1004.d0
  real,parameter :: Rd  = 287.d0
  real,parameter :: Lv  = 2.500E+6
  real,parameter :: eps = 0.622
  real,parameter :: gamd = 9.8000E-3
  const=Lv**2/(Cpd*Rd)  
  
  !## read in-file
  read(5,*)itime
  read(5,*)ilon,ilat,ihgt,ilay
  allocate( &
       tmp2(ilon,ilat,ihgt),pres(ilon,ilat,ihgt),&
       tmp1(ilon,ilat,ihgt),tmp1v(ilon,ilat,ihgt),to3v(ilon,ilat,ihgt),&
       eqpt(ilon,ilat,ilay),seqpt2(ilon,ilat,ihgt),&
       zlcl(ilon,ilat,ilay),sh(ilon,ilat,ihgt),vtm(ilon,ilat,ihgt),&
       zlev(ihgt),pe(ihgt),pev(ihgt),dz(ihgt),tdif(ihgt),tdifv(ihgt),&
       to3(ilon,ilat,ihgt),hgtos(ilon,ilat),presos(ilon,ilat),&
       !cape(ilon,ilat),cin(ilon,ilat),lnb(ilon,ilat),lfc(ilon,ilat),&
       capev(ilon,ilat),cinv(ilon,ilat),lnbv(ilon,ilat),lfcv(ilon,ilat),&
       lcl(ilon,ilat),eqpto(ilon,ilat)&
       )
  read(5,*)zlev
  read(5,*)undef
  read(5,*)path
  read(5,*)var_tmp2,var_tmp1
  read(5,*)var_eqpt,var_sh,var_seqpt2
  read(5,*)var_pres,var_zlcl
  read(5,*)var_cape,var_to3
  !read(5,*)var_capev,var_to3v

  !## open
  open(1,file=trim(path)//"/"//trim(var_tmp1),status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*ihgt*4)
  open(2,file=trim(path)//"/"//trim(var_tmp2),status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*ihgt*4)
  open(3,file=trim(path)//"/"//trim(var_eqpt),status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*ilay*4)
  open(4,file=trim(path)//"/"//trim(var_seqpt2),status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*ihgt*4)
  open(7,file=trim(path)//"/"//trim(var_pres),status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*ihgt*4)
  open(8,file=trim(path)//"/"//trim(var_zlcl),status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*ilay*4)
  open(9,file=trim(path)//"/"//trim(var_sh),status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*ihgt*4)

!cape cin lfc lnb capev cinv lfcv lnbv lcl hgtos presos 11vars
  open(11,file=trim(path)//"/"//trim(var_cape),status="replace",&
       access="direct",form="unformatted",recl=ilon*ilat*7*4)
!tmp1 to3 tmp1v to3v 4vars
  open(12,file=trim(path)//"/"//trim(var_to3),status="replace",&
       access="direct",form="unformatted",recl=ilon*ilat*ihgt*4*4)
  
  !## start calc loop
  !###########################################################
  do it=1,itime
     print *, "now calculating ", it, "/", itime
     read(1,rec=it) tmp1   !! real temperature
     read(2,rec=it) tmp2   !! to2 (bad accuracy)
     read(3,rec=it) eqpt   !! equivalent pot. 3D or 2D(ml)
     read(4,rec=it) seqpt2 !! saturated eqpt.
     read(7,rec=it) pres   !! press at isohight
     read(8,rec=it) zlcl   !! zlcl 3D or 2D(ml)
     read(9,rec=it) sh     !! specific humidity for virtual
     
     !## second calculation
     !###########################################################

     !make dz (const.)
     dz(:)=500.

     !init
     !cape(:,:)=0. ; cin(:,:)=0.
     capev(:,:)=0. ; cinv(:,:)=0.

     do j=1,ilat
        do i=1,ilon

           sz=0 ; flag=0
           ez=0 ; uflag=0
           
           do k=1,ihgt
              
              !make dz (vari.)
              !if(k.eq.1)then
              !   dz(k)=zlev(k+1)-zlev(k)
              !else if(k.eq.ihgt)then
              !   dz(k)=zlev(k)-zlev(k-1)
              !else
              !   dz(k)=(zlev(k+1)-zlev(k-1))/2.
              !end if

              !find initial layer (sz)
              if(ilay.eq.1)then
                 ! ml
                 sz=2
                 sl=1                 
              else
                 ! sb 5 mu
                 if(tmp2(i,j,k).ne.undef.and.flag.eq.0)then
                    sz=k
                    sl=k
                    flag=1
                 end if
              end if

              !find top layer (ez)
              if(tmp2(i,j,k).ne.undef.and.k.ne.ihgt)then
                 if(tmp2(i,j,k+1).eq.undef)then
                    ez=k
                 end if
              end if
              if(tmp2(i,j,ihgt).ne.undef)then
                 ez=ihgt
              end if
              
              !make virtual tmp. (ambient)
              if(sh(i,j,k).ne.undef)then
                 mxr_a=sh(i,j,k)/(1-sh(i,j,k))
              else
                 mxr_a=0.
              end if

              if(tmp1(i,j,k).ne.undef)then
                 tmp1v(i,j,k)=tmp1(i,j,k)*(1+0.61*mxr_a)
              else
                 tmp1v(i,j,k)=undef
              end if
              
           end do !k

           ! undef all layers
           if(sz.eq.0.or.ez.eq.0) then
              uflag=1
              capev(i,j)=undef ;! cape(i,j)=undef
              cinv(i,j)=undef  ;! cin(i,j)=undef
              lfcv(i,j)=undef  ;! lfc(i,j)=undef
              lnbv(i,j)=undef  ;! lnb(i,j)=undef
              lcl(i,j)=undef
              hgtos(i,j)=undef
              presos(i,j)=undef
           end if
           if(uflag.eq.1) cycle

           ! define lifting layers values
           lcl(i,j)=zlcl(i,j,sl)
           eqpto(i,j)=eqpt(i,j,sl)
           hgtos(i,j)=zlev(sz)
           presos(i,j)=pres(i,j,sz)
           
           if(sz.ge.2)then
              to3(i,j,1:sz-1)=undef
              to3v(i,j,1:sz-1)=undef
              tdif(1:sz-1)=undef
           end if
           
           if(ez.le.ihgt-1)then
              to3(i,j,ez+1:ihgt)=undef
              to3v(i,j,ez+1:ihgt)=undef
              tdif(ez+1:ihgt)=undef
           end if

           !## calculate buoyancy with secondary lifted parcel
           do k=sz,ez
              
              !! lifting variable
              !teten's formula
              es=6.11*exp(17.27*(tmp2(i,j,k)-273.15)/(tmp2(i,j,k)-35.86))
              qvs=eps*es/(pres(i,j,k)-es)
              varii=(eps+qvs)*qvs/(tmp2(i,j,k)**2)
              pot2=tmp2(i,j,k)*((1000./pres(i,j,k))**(Rd/Cpd))
              !! ambient variable
              pot1=tmp1(i,j,k)*((1000./pres(i,j,k))**(Rd/Cpd))
              
              
              !secondary lifting parcel              
              if(zlev(k).le.lcl(i,j)) then

                 ! below lcl
                 to3(i,j,k)=tmp2(i,j,sz)-(zlev(k)-zlev(sz))*gamd
                 poto3=to3(i,j,k)*((1000./pres(i,j,k))**(Rd/Cpd))
                 
                 !tdif(k)=to3(i,j,k)-tmp1(i,j,k)
                 !pe(k)=g*dz(k)*tdif(k)/tmp1(i,j,k)
                 
                 ! virtual tmp.
                 mxr_o=sh(i,j,sz)/(1-sh(i,j,sz))
                 to3v(i,j,k)=to3(i,j,k)*(1+0.61*mxr_o)
                 
                 tdifv(k)=to3v(i,j,k)-tmp1v(i,j,k)
                 pev(k)=g*dz(k)*tdifv(k)/tmp1v(i,j,k)
                 
              else

                 ! avobe lcl
                 !## calc lifted air temp. (good accuracy)
                 dptes=eqpto(i,j)-seqpt2(i,j,k)
                 poto3=pot2+dptes/(1+const*varii)
                 to3(i,j,k)=poto3/((1000./pres(i,j,k))**(Rd/Cpd))
                 
                 !tdif(k)=to3(i,j,k)-tmp1(i,j,k)
                 !pe(k)=g*dz(k)*tdif(k)/tmp1(i,j,k)
                 
                 ! virtual tmp.
                 es3=6.11*exp(17.27*(to3(i,j,k)-273.15)/(to3(i,j,k)-35.86))
                 mxr_o=eps*es3/(pres(i,j,k)-es3)
                 to3v(i,j,k)=to3(i,j,k)*(1+0.61*mxr_o)
                 
                 tdifv(k)=to3v(i,j,k)-tmp1v(i,j,k)
                 pev(k)=g*dz(k)*tdifv(k)/tmp1v(i,j,k)

              end if
              
           end do !k

           !!## check LFC and LNB (see below)
           !call chk_lfclnb(sz,ez,ihgt,undef,tdif,zlev,lfcp,lnbp)
           !lfc(i,j)=lfcp ; lnb(i,j)=lnbp

           call chk_lfclnb(sz,ez,ihgt,undef,tdifv,zlev,lfcp,lnbp)
           lfcv(i,j)=lfcp ; lnbv(i,j)=lnbp
           
           !## integrate to cape cin
           do k=sz+1,ihgt

              !! actual tmp.
              !if(lfc(i,j).ne.undef)then
              !   if (pe(k).le.0..and.zlev(k).le.lfc(i,j))then
              !      cin(i,j)=cin(i,j)+abs(pe(k))
              !   end if
              !else
              !   cin(i,j) = undef
              !end if
              
              !!if(pe(k).ge.0..and.zlev(k).ge.lfc(i,j).and.&
              !!zlev(k).le.lnb(i,j)) then
              !if(pe(k).ge.0..and.zlev(k).ge.lfc(i,j)) then
              !   cape(i,j) = cape(i,j) + abs(pe(k))
              !end if
              
              ! virtual tmp.
              if(lfcv(i,j).ne.undef)then
                 if (pev(k).le.0..and.zlev(k).le.lfcv(i,j))then
                    cinv(i,j)=cinv(i,j)+abs(pev(k))
                 end if
              else
                 cinv(i,j) = undef
              end if
              
              !if(pev(k).ge.0..and.zlev(k).ge.lfcv(i,j).and.&
              !zlev(k).le.lnbv(i,j)) then              
              if(pev(k).ge.0..and.zlev(k).ge.lfcv(i,j)) then
                 capev(i,j) = capev(i,j) + abs(pev(k))
              end if
              
           end do
           
        end do !i
     end do !j
     
!     write(11,rec=it) cape,cin,lfc,lnb,capev,cinv,lfcv,lnbv,lcl,hgtos,presos
     write(11,rec=it) capev,cinv,lfcv,lnbv,lcl,hgtos,presos
!     write(12,rec=it) cin
!     write(13,rec=it) lfc
!     write(14,rec=it) lnb
     write(12,rec=it) tmp1,to3,tmp1v,to3v
!     write(15,rec=it) tmp1
!     write(15,rec=it) to3
!     write(15,rec=it) tmp1v
!     write(15,rec=it) to3v    
     
  end do
  
  stop
end program main

!##############################################################
!## check LFC & LNB
!
! LFC : at second layer (sz+1), tdif must be negative, and
!      above lfc, !"tdif must be positive at least 2 sequential
!      layers"!
!      if tdif is positive at sz+1 , d-lfc is set to be zero
! LNB : it must have lfc, !"and ,before lnb, tdif must be posi-
!      tive at least sequential 2 layers"!
!       if multiple lnbs, the most highest one is to be used
! * !" ~ "! comment out
!
! 2018.04.19 ks
!##############################################################
subroutine chk_lfclnb(sz,ez,ihgt,undef,tdif,zlev,lfcp,lnbp)  
  implicit none
  integer,intent(in) :: sz,ez,ihgt
  real,intent(in) :: tdif(ihgt),zlev(ihgt)
  real,intent(in) :: undef
  real,intent(out) :: lfcp,lnbp
  integer :: lnbflag,lfcflag
  integer :: k
  lnbflag=0 ; lfcflag=0
  
  do k=sz+1,ez-1
     if ( tdif(k).ne.undef.and.tdif(k+1).ne.undef ) then
        
        ! lfc
        !if ( tdif(sz+1).lt.0..and.tdif(k)*tdif(k+1).le.0. &
        !.and.tdif(k)*tdif(k+2).le.0..and.lfcflag.eq.0 ) then
        if ( tdif(sz+1).lt.0..and.tdif(k)*tdif(k+1).le.0. &
             .and.lfcflag.eq.0 ) then           
           lfcflag = 1
           lfcp=(zlev(k+1)*abs(tdif(k))+zlev(k)*abs(tdif(k+1)))&
                /(abs(tdif(k))+abs(tdif(k+1)))
        else if (tdif(sz+1).ge.0.)then
           lfcflag = 1
           lfcp=zlev(sz)
        end if
        
        ! lnb
        !if ( tdif(k-1).gt.0..and.tdif(k).gt.0..and.&
        !tdif(k+1).le.0..and.lnbflag.eq.0.and.lfcflag.ne.0 ) then
        if ( tdif(k).gt.0..and.tdif(k+1).le.0..and.lfcflag.ne.0 ) then
           lnbflag=1
           lnbp=(zlev(k+1)*abs(tdif(k))+zlev(k)*abs(tdif(k+1)))&
                /(abs(tdif(k))+abs(tdif(k+1)))
        end if
        
     end if
  end do !k
  
  !## check awkward profile
  
  ! always negative tdif
  if( lfcflag .eq. 0 ) then
     lfcp = undef
     lnbp = undef
  end if
  
  ! lnb beyond ihgt
  if( lfcflag.ne.0.and.lnbflag.eq.0) then  
     lnbp = zlev(ihgt)
     lnbflag = ihgt
  end if
  
end subroutine chk_lfclnb

