#!/bin/bash
## otegaru kishoukaiseki script
## using MSM analysis data
##=== vr.1.0 ===
## 2014/12/08 Corded by Yusuke KIMURA
##     for prs
##        T-Z-U-V, RH-U-V,OMG-U-V, VOR-U-V, DIV-U-V
##        925-,850-,700-,500-,200-,hPa
##     for surf
##        SAT-SLP-U-V, VOR-U-V, DIV-U-V, CLA
##=== vr.1.1 ===
## 2015/09/28 for English by kasuga satoru
##=== vr.2.0 ===
## 2015/09/28 ks
##     changed loop structure and color scheme for 'reference time'
##     discarded get_time.gs

PARMF="../parm.sh"
. $PARMF

STEP=draw_jmbsc_msm

if [ ${YYS}${MMS}${DDS} -lt "20170331" ] ;then
    vvff=1
else
    vvff=0
fi

for iz in `seq 16`; do

lev=`echo $LEVELS | cut -d" " -f $iz`

if [ -z $lev ]; then
    break
fi


## Make gs-file
#++ for prs
cat << EOF > ${GSDIR}/draw_jmbsc_msm2.gs
** Draw MSM pall and surf
** 2014/12/08 Corded by Yusuke KIMURA
** 2018/04/12 ks

function draw_msm( args )

*## Get args
*   =subwrd( args,1 )
*## vvel flag
vvff=$vvff
eps=$EPS
density=600
size=600

IMGDIR1="$IMGDIR/jmbsc_msm/TMP"
IMGDIR3="$IMGDIR/jmbsc_msm/RH"
IMGDIR4="$IMGDIR/jmbsc_msm/DIV"
IMGDIR5="$IMGDIR/jmbsc_msm/VOR"
IMGDIR6="$IMGDIR/jmbsc_msm/OMG"
IMGDIR7="$IMGDIR/jmbsc_msm/CLA"
if(vvff=1)
'!mkdir -p 'IMGDIR1' 'IMGDIR3' 'IMGDIR4' 'IMGDIR5' 'IMGDIR6' 'IMGDIR7
else
'!mkdir -p 'IMGDIR1' 'IMGDIR3' 'IMGDIR4' 'IMGDIR5' 'IMGDIR7
endif

*## Parameter
DIRNAME  = "jmbsc_msm"
LATLEVS  = "$LATSI $LATEI"
LONLEVS  = "$LONSI $LONEI"
DATEK    = "$DATEK"

'reinit'
*pall
'open $MSMBIN/HGTprs.ctl'   ;*1
'open $MSMBIN/TMPprs.ctl'   ;*2
'open $MSMBIN/RHprs.ctl'    ;*3
'open $MSMBIN/UGRDprs.ctl'  ;*4
'open $MSMBIN/VGRDprs.ctl'  ;*5

*surf
'open $MSMBIN/PRMSLmsl.ctl' ;*6
'open $MSMBIN/TMP2m.ctl'    ;*7
'open $MSMBIN/RH2m.ctl'     ;*8
'open $MSMBIN/UGRD10m.ctl'  ;*9
'open $MSMBIN/VGRD10m.ctl'  ;*10
'open $MSMBIN/TCDCsfc.ctl'  ;*11
'open $MSMBIN/PRESsfc.ctl'  ;*12

if(vvff=1)
'open $MSMBIN/VVELprs.ctl'  ;*13
endif

'set lat 'LATLEVS
'set lon 'LONLEVS
'set xlopts 1 5 0.15'
'set ylopts 1 5 0.15'
'set map 1 1 7'
'set mpdset $mpdset'

*z=1;while(z<=5)
*if(z=1);lev=925;endif
*if(z=2);lev=850;endif
*if(z=3);lev=700;endif
*if(z=4);lev=500;endif
*if(z=5);lev=200;endif
lev=$lev
'set lev 'lev
'mask=lterp(pressfc.12/100,hgtprs,aave)-'lev

'set time 'DATEK

** +++ TMP +++
'd maskout(tmpprs.2-273.15,mask)'
min=subwrd(result,2)
max=subwrd(result,4)
int=subwrd(result,6)
   CLEVS1  = min' 'max' 'int
   CKIND1  = 'grainbow'
   CGXOUT1 = 'shaded'
** +++ HGT  +++   
'd maskout(hgtprs.1,mask)'
min=subwrd(result,2)
max=subwrd(result,4)
int=subwrd(result,6)
min=min-int
max=max+int
   CLEVS2  = min' 'max' 'int
   CINT2   = int
*   CKIND2  = 'black->black'
*   CKIND2  = 'rainbow'
   CKIND2  = 'blue->black->red'
   CGXOUT2 = 'contour'
** +++ RH +++
*NaN (color-rh.gs) 
** +++ DIV +++
'd maskout(hdivg(ugrdprs.4,vgrdprs.5),mask)*1e5'
min=subwrd(result,2)
max=subwrd(result,4)
int=subwrd(result,6)
if(-min>=max);max=-min;endif
if(-min<=max);min=-max;endif
   CLEVS4  = min' 'max' 'int
   CKIND4  = 'purple->white->green'
   CGXOUT4 = 'shaded'
** +++ VOR +++
'd maskout(hcurl(ugrdprs.4,vgrdprs.5),mask)*1e5'
min=subwrd(result,2)
max=subwrd(result,4)
int=subwrd(result,6)
if(-min>=max);max=-min;endif
if(-min<=max);min=-max;endif
   CLEVS5  = min' 'max' 'int
   CKIND5  = 'blue->white->red'
   CGXOUT5 = 'shaded'
** +++ OMG +++
if(vvff=1)
'd maskout(vvelprs.13,mask)'
min=subwrd(result,2)
max=subwrd(result,4)
int=subwrd(result,6)
ff=substr(min,1,1)
if(ff!=f)
if(-min>=max);max=-min;endif
if(-min<=max);min=-max;endif
   CLEVS6  = min' 'max' 'int
else
vvflag=1
endif
   CKIND6  = 'red->white->blue'
   CGXOUT6 = 'shaded'
endif
** +++ WIND +++
* check vector int
'q dim'
xline=sublin(result,2)
yline=sublin(result,3)
sx=subwrd(xline,11)
ex=subwrd(xline,13)
sy=subwrd(yline,11)
ey=subwrd(yline,13)
'd 'ex'-'sx
dx=subwrd(result,4)
'd 'ey'-'sy
dy=subwrd(result,4)
if(dx>=dy);md=dx;else;md=dy;endif
'd 'md'/25'
dd=subwrd(result,4)
vi=math_nint(dd)
vis=math_nint(dd/2)
***
'd maskout(mag(ugrdprs.4,vgrdprs.5),mask)'
min=subwrd(result,2)
max=subwrd(result,4)
int=subwrd(result,6)
arrmax=max-int
   ARRSCL  = '0.3 'arrmax
   ARRHEAD = '0.1'
   ARRTHCK = '5'
   DRWVCT1 = 'skip(ugrdprs.4,'vi','vi');maskout(vgrdprs.5,mask)'
** ++++++++++++   
*for surf
z=$iz
if(z=1)
** +++ TEMP +++
'd tmp2m.7-273.15'
min=subwrd(result,2)
max=subwrd(result,4)
int=subwrd(result,6)
   CLEVS1S  = min' 'max' 'int
** +++ SLP +++   
'd prmslmsl.6/100'
min=subwrd(result,2)
max=subwrd(result,4)
int=subwrd(result,6)
min=min-int
max=max+int
   CINT2S   = int
   CLEVS2S  = min' 'max' 'int
** +++ RH +++
*NaN (color-rh.gs)
** +++ DIV +++
'd hdivg(ugrd10m.9,vgrd10m.10)*1e5'
min=subwrd(result,2)
max=subwrd(result,4)
int=subwrd(result,6)
if(-min>=max);max=-min;endif
if(-min<=max);min=-max;endif
   CLEVS4S  = min' 'max' 'int
** +++ VOR +++
'd hcurl(ugrd10m.9,vgrd10m.10)*1e5'
min=subwrd(result,2)
max=subwrd(result,4)
int=subwrd(result,6)
if(-min>=max);max=-min;endif
if(-min<=max);min=-max;endif
   CLEVS5S  = min' 'max' 'int
** +++ CLA +++
   CLEVS7  = '0 100 10'
   CKIND7  = 'black->white'
   CGXOUT7 = 'shaded'
** +++ WIND +++
'd mag(ugrd10m.9,vgrd10m.10)'
min=subwrd(result,2)
max=subwrd(result,4)
int=subwrd(result,6)
arrmax=max-int
   ARRSCLS  = '0.3 'arrmax
   DRWVCT1S = 'skip(ugrd10m.9,'vi','vi');vgrd10m.10'
'set grads off';'c';'set grads off'
** ++++++++++++
endif

** time loop
** search tdef
l=1;while(l<=15)
'q ctlinfo'
line=sublin(result,l)
if(subwrd(line,1)=tdef)
et=subwrd(line,2)
break
endif
l=l+1;endwhile

t=1;while(t<=et)
jt=t+3
'set t 'jt

* make jst
'q dim'
line=sublin(result,5)
jst=subwrd(line,6)
year=substr(jst,9,4)
emon=substr(jst,6,3)
day =substr(jst,4,2)
hour=substr(jst,1,2)

if(emon=JAN);nmon="01";endif
if(emon=FEB);nmon="02";endif
if(emon=MAR);nmon="03";endif
if(emon=APR);nmon="04";endif
if(emon=MAY);nmon="05";endif
if(emon=JUN);nmon="06";endif
if(emon=JUL);nmon="07";endif
if(emon=AUG);nmon="08";endif
if(emon=SEP);nmon="09";endif
if(emon=OCT);nmon="10";endif
if(emon=NOV);nmon="11";endif
if(emon=DEC);nmon="12";endif

*tdate=year'/'nmon'/'day' 'hour'JST (t='t')'
tdate=year'/'nmon'/'day' 'hour'JST'
fdate=year''nmon''day''hour'JST'
'set t 't

'set grads off';'c';'set grads off'


*****************************
** draw
*****************************
** pall
** +++ D TZUV +++
   'color 'CLEVS1' -kind 'CKIND1' -gxout 'CGXOUT1
   'd maskout(tmpprs.2-273.15,mask)'
   'cbarn'
   'color 'CLEVS1' -kind white->white -gxout contour'
   'set cthick 1'
   'set clab off'
   'set cstyle 1'
   'd maskout(tmpprs.2-273.15,mask)'
   'set gxout vector'
   'set ccolor 1'
   'set arrscl 'ARRSCL
   'set cthick 'ARRTHCK
   'set arrowhead 'ARRHEAD
   'd 'DRWVCT1
*   'color 'CLEVS2' -kind 'CKIND2' -gxout 'CGXOUT2
   'set gxout contour'
   'set clab off'
   'set cint 'CINT2
   'set ccolor 0'
   'set cthick 15'
   'd maskout(hgtprs.1,mask)'
*   'color 'CLEVS2' -kind 'CKIND2' -gxout 'CGXOUT2
   'set clab on'
   'set ccolor 1' 
   'set cthick 6'
   'd maskout(hgtprs.1,mask)'
*   'draw title otgrMSM 'lev'hPa TMP[degC] HGT[gpm] WIND[m/s] \ 'tdate
   'draw title 'lev'hPa TMP[degC] HGT[gpm] WIND[m/s] \ 'tdate
if(eps=1)
   'print -R tmp.eps'
   '!convert -density 'density' -resize 'size' +antialias tmp.eps 'IMGDIR1'/'DIRNAME'_pall_TZUV'lev'_'fdate'.png'
else
   'printim 'IMGDIR1'/'DIRNAME'_pall_TZUV'lev'_'fdate'.png x1100 y850 white'
endif
'set grads off';'c';'set grads off'
** +++ D RH-UV +++
if(lev>=200)
'color 0 100 10 -kind white->lightgray->skyblue->mediumpurple'
   'd maskout(rhprs.3,mask)'
   'cbarn'
   'color 0 100 10 -kind white->white -gxout contour'
   'set cthick 1'
   'set clab off'
   'set cstyle 1'
   'd maskout(rhprs.3,mask)'
   'set gxout vector'
   'set ccolor 1'
   'set arrscl 'ARRSCL
   'set cthick 'ARRTHCK
   'set arrowhead 'ARRHEAD
   'd 'DRWVCT1
*   'draw title otgrMSM 'lev'hPa RH[%] WIND[m/s] \ 'tdate
   'draw title 'lev'hPa RH[%] WIND[m/s] \ 'tdate
if(eps=1)
   'print -R tmp.eps'
   '!convert -density 'density' -resize 'size' +antialias tmp.eps 'IMGDIR3'/'DIRNAME'_pall_RHUV'lev'_'fdate'.png'
else
   'printim 'IMGDIR3'/'DIRNAME'_pall_RHUV'lev'_'fdate'.png x1100 y850 white'
endif
'set grads off';'c';'set grads off'
endif
** +++ D DIV-UV +++
   'set lon 120 150'
   'set lat 22.4 47.6'
   'tmpdiv=hdivg(maskout(ugrdprs.4,mask),vgrdprs.5)*1e5'
   'color 'CLEVS4' -kind 'CKIND4' -gxout 'CGXOUT4
*   'd hdivg(maskout(ugrdprs.4,mask),vgrdprs.5)*1e5'   
   'set lat 'LATLEVS
   'set lon 'LONLEVS
   'd tmpdiv'
   'cbarn'
   'color 'CLEVS4' -kind white->white -gxout contour'
   'set cthick 1'
   'set clab off'
   'set cstyle 1'
*   'd hdivg(maskout(ugrdprs.4,mask),vgrdprs.5)*1e5'
   'd tmpdiv'
   'set gxout vector'
   'set ccolor 1'
   'set arrscl 'ARRSCL
   'set cthick 'ARRTHCK
   'set arrowhead 'ARRHEAD
   'd 'DRWVCT1
*   'draw title otgrMSM 'lev'hPa DIV[10^-5/s] WIND[m/s] \ 'tdate
   'draw title 'lev'hPa DIV[10^-5/s] WIND[m/s] \ 'tdate
if(eps=1)
   'print -R tmp.eps'
   '!convert -density 'density' -resize 'size' +antialias tmp.eps 'IMGDIR4'/'DIRNAME'_pall_DIVUV'lev'_'fdate'.png'
else
   'printim 'IMGDIR4'/'DIRNAME'_pall_DIVUV'lev'_'fdate'.png x1100 y850 white'
endif
'set grads off';'c';'set grads off'
** +++ D VOR-UV +++
   'set lon 120 150'
   'set lat 22.4 47.6'
   'tmpvor=hcurl(maskout(ugrdprs.4,mask),vgrdprs.5)*1e5'
   'color 'CLEVS5' -kind 'CKIND5' -gxout 'CGXOUT5
*   'd hcurl(maskout(ugrdprs.4,mask),vgrdprs.5)*1e5'
   'set lat 'LATLEVS
   'set lon 'LONLEVS
   'd tmpvor'
   'cbarn'
   'color 'CLEVS5' -kind white->white -gxout contour'
   'set cthick 1'
   'set clab off'
   'set cstyle 1'
*   'd hcurl(maskout(ugrdprs.4,mask),vgrdprs.5)*1e5'
   'd tmpvor'
   'set gxout vector'
   'set ccolor 1'
   'set arrscl 'ARRSCL
   'set cthick 'ARRTHCK
   'set arrowhead 'ARRHEAD
   'd 'DRWVCT1
*   'draw title otgrMSM 'lev'hPa VOR[10^-5 1/s] WIND[m/s] \ 'tdate
   'draw title 'lev'hPa VOR[10^-5 1/s] WIND[m/s] \ 'tdate
if(eps=1)
   'print -R tmp.eps'
   '!convert -density 'density' -resize 'size' +antialias tmp.eps 'IMGDIR5'/'DIRNAME'_pall_VORUV'lev'_'fdate'.png'
else
   'printim 'IMGDIR5'/'DIRNAME'_pall_VORUV'lev'_'fdate'.png x1100 y850 white'
endif
'set grads off';'c';'set grads off'
** +++ D OMG-UV +++
if(vvff=1)
if(vvflag!=1)
   'color 'CLEVS6' -kind 'CKIND6' -gxout 'CGXOUT6
   'd maskout(vvelprs.13,mask)'
   'cbarn'
   'color 'CLEVS6' -kind white->white -gxout contour'
   'set cthick 1'
   'set clab off'
   'set cstyle 1'
   'd maskout(vvelprs.13,mask)'
   'set gxout vector'
   'set ccolor 1'
   'set arrscl 'ARRSCL
   'set cthick 'ARRTHCK
   'set arrowhead 'ARRHEAD
   'd 'DRWVCT1
*   'draw title otgrMSM 'lev'hPa OMG[Pa/s] WIND[m/s] \ 'tdate
   'draw title 'lev'hPa OMG[Pa/s] WIND[m/s] \ 'tdate
if(eps=1)
   'print -R tmp.eps'
   '!convert -density 'density' -resize 'size' +antialias tmp.eps 'IMGDIR6'/'DIRNAME'_pall_OMGUV'lev'_'fdate'.png'
else
   'printim 'IMGDIR6'/'DIRNAME'_pall_OMGUV'lev'_'fdate'.png x1100 y850 white'
endif
'set grads off';'c';'set grads off'
endif
endif
** +++++++++++++++++

*for surf
if(z=1)
** +++ D TZUV +++
   'color 'CLEVS1S' -kind 'CKIND1' -gxout 'CGXOUT1
   'd tmp2m.7-273.15'
   'cbarn'
   'color 'CLEVS1S' -kind white->white -gxout contour'
   'set cthick 1'
   'set clab off'
   'set cstyle 1'
   'd tmp2m.7-273.15'
   'set gxout vector'
   'set ccolor 1'
   'set arrscl 'ARRSCL
   'set cthick 'ARRTHCK
   'set arrowhead 'ARRHEAD
   'd 'DRWVCT1S
*   'color 'CLEVS2S' -kind 'CKIND2' -gxout 'CGXOUT2
   'set gxout contour'
   'set cint 'CINT2S
   'set clab off'
   'set ccolor 0'
   'set cthick 15'
   'd prmslmsl.6/100'
   'color 'CLEVS2S' -kind 'CKIND2' -gxout 'CGXOUT2
*   'set cint 'CINT2S
   'set clab on'
*   'set ccolor 15'
   'set cthick 4'
   'd prmslmsl.6/100'
*   'draw title otgrMSM SFC T2m[degC] SLP[hPa] WIND10m[m/s] \ 'tdate
   'draw title SFC T2m[degC] SLP[hPa] WIND10m[m/s] \ 'tdate
if(eps=1)
   'print -R tmp.eps'
   '!convert -density 'density' -resize 'size' +antialias tmp.eps 'IMGDIR1'/'DIRNAME'_surf_SATSLPUV_'fdate'.png'
else
   'printim 'IMGDIR1'/'DIRNAME'_surf_SATSLPUV_'fdate'.png x1100 y850 white'
endif
'set grads off';'c';'set grads off'
** +++ D RH-UV +++
'color 0 100 10 -kind white->lightgray->skyblue->mediumpurple'
   'd rh2m.8'
   'cbarn'
   'color 0 100 10 -kind white->white -gxout contour'
   'set cthick 1'
   'set clab off'
   'set cstyle 1'
   'd rh2m.8'
   'set gxout vector'
   'set ccolor 1'
   'set arrscl 'ARRSCLS
   'set cthick 'ARRTHCK
   'set arrowhead 'ARRHEAD
   'd 'DRWVCT1S
*   'draw title otgrMSM SFC RH2m[%] WIND10m[m/s] \ 'tdate
   'draw title SFC RH2m[%] WIND10m[m/s] \ 'tdate
if(eps=1)
   'print -R tmp.eps'
   '!convert -density 'density' -resize 'size' +antialias tmp.eps 'IMGDIR3'/'DIRNAME'_surf_RHUV_'fdate'.png'
else
   'printim 'IMGDIR3'/'DIRNAME'_surf_RHUV_'fdate'.png x1100 y850 white'
endif
'set grads off';'c';'set grads off'
** +++ D DIV-UV +++
   'set lon 120 150'
   'set lat 22.4 47.6'
   'tmpdiv=hdivg(ugrd10m.9,vgrd10m.10)*1e5'
   'color 'CLEVS4S' -kind 'CKIND4' -gxout 'CGXOUT4
*   'd hdivg(ugrd10m.9,vgrd10m.10)*1e5'
   'set lat 'LATLEVS
   'set lon 'LONLEVS
   'd tmpdiv'
   'cbarn'
   'color 'CLEVS4S' -kind white->white -gxout contour'
   'set clab off'
   'set cstyle 1'
   'set cthick 1'
*   'd hdivg(ugrd10m.9,vgrd10m.10)*1e5'
   'd tmpdiv'
   'set gxout vector'
   'set ccolor 1'
   'set arrscl 'ARRSCLS
   'set cthick 'ARRTHCK
   'set arrowhead 'ARRHEAD
   'd 'DRWVCT1S
*   'draw title otgrMSM SFC DIV[10^-5 1/s] WIND10m[m/s] \ 'tdate
   'draw title SFC DIV[10^-5 1/s] WIND10m[m/s] \ 'tdate
if(eps=1)
   'print -R tmp.eps'
   '!convert -density 'density' -resize 'size' +antialias tmp.eps 'IMGDIR4'/'DIRNAME'_surf_DIVUV_'fdate'.png'
else
   'printim 'IMGDIR4'/'DIRNAME'_surf_DIVUV_'fdate'.png x1100 y850 white'
endif
'set grads off';'c';'set grads off'
** +++ D VOR-UV +++
   'set lon 120 150'
   'set lat 22.4 47.6'
   'tmpvor=hcurl(ugrd10m.9,vgrd10m.10)*1e5'
   'color 'CLEVS5S' -kind 'CKIND5' -gxout 'CGXOUT5
*   'd hcurl(ugrd10m.9,vgrd10m.10)*1e5'
   'set lat 'LATLEVS
   'set lon 'LONLEVS
   'd tmpvor'
   'cbarn'
   'color 'CLEVS5S' -kind white->white -gxout contour'
   'set clab off'
   'set cstyle 1'
   'set cthick 1'
*   'd hcurl(ugrd10m.9,vgrd10m.10)*1e5'
   'd tmpvor'
   'set gxout vector'
   'set ccolor 1'
   'set arrscl 'ARRSCL
   'set cthick 'ARRTHCK
   'set arrowhead 'ARRHEAD
   'd 'DRWVCT1
*   'draw title otgrMSM SFC VOR[10^-5 1/s] WIND10m[m/s] \ 'tdate
   'draw title SFC VOR[10^-5 1/s] WIND10m[m/s] \ 'tdate
if(eps=1)
   'print -R tmp.eps'
   '!convert -density 'density' -resize 'size' +antialias tmp.eps 'IMGDIR5'/'DIRNAME'_surf_VORUV_'fdate'.png'
else
   'printim 'IMGDIR5'/'DIRNAME'_surf_VORUV_'fdate'.png x1100 y850 white'
endif
'set grads off';'c';'set grads off'
** +++ D CLA +++
   'color 'CLEVS7' -kind 'CKIND7' -gxout 'CGXOUT7
   'd tcdcsfc.11'
   'cbarn'
*   'set 3 1 7'
   'draw map'
*   'draw title otgrMSM SFC Surface-Total-Cloud-Cover[%] \ 'tdate
   'draw title SFC Surface-Total-Cloud-Cover[%] \ 'tdate
if(eps=1)
   'print -R tmp.eps'
   '!convert -density 'density' -resize 'size' +antialias tmp.eps 'IMGDIR7'/'DIRNAME'_surf_CLA_'fdate'.png'
else
   'printim 'IMGDIR7'/'DIRNAME'_surf_CLA_'fdate'.png x1100 y850 white'
endif
'set grads off';'c';'set grads off'
** +++++++++++++
endif

t=t+1;endwhile
*z=z+1;endwhile

if(eps=1)
'!rm tmp.eps'
endif

'quit'
exit

EOF




## RUN gs-files
cd ${GSDIR}
#files=`ls *.png | wc -l`
#if [ $files -ne 0 ]; then
#    mkdir -p org_image
#    mv *.png org_image/.
#fi

grads -blc "draw_jmbsc_msm2.gs"
#grads -blc "draw_jmbsc_msm2.gs" 1>/dev/null 2>/dev/null 

done

cd ${CRTDIR}
exit

