#!/bin/bash
## otegaru kishoukaiseki script
## using MSM analysis data
##=== vr.1.0 ===
## 2014/12/08 Corded by Yusuke KIMURA
##     for prs
##        T-Z-U-V, RH-U-V,OMG-U-V, VOR-U-V, DIV-U-V
##        925-,850-,700-,500-,200-,hPa
##     for surf
##        SAT-SLP-U-V, VOR-U-V, DIV-U-V, CLA
##=== vr.1.1 ===
## 2015/09/28 for English by kasuga satoru
##=== vr.2.0 ===
## 2018/04/14 ks
##     changed loop structure and color scheme for 'reference time'
##     discarded get_time.gs
##=== vr.2.0x ===
## 2018/04/24 ks
##     for X analysis

PARMF="../parm.sh"
. $PARMF

STEP=draw_jmbsc_msm2-x


## Make gs-file
#++ for prs
cat << EOF > ${GSDIR}/draw_jmbsc_msm2-x2.gs
** Draw MSM pall and surf
** 2014/12/08 Corded by Yusuke KIMURA
** 2018/04/12 ks

function draw_msm( args )

*## Get args
*   =subwrd( args,1 )

IMGDIR1="$IMGDIR/jmbsc_msm/CAPE"
'!mkdir -p 'IMGDIR1


*## Parameter
DIRNAME  = "jmbsc_msm"
LATLEVS  = "$LATSI $LATEI"
LONLEVS  = "$LONSI $LONEI"
DATEK    = "$DATEK"

'reinit'

v=1;while(v<=2)

if(v=1);'open $MSMBIN/CAPE5.ctl';vv=5;endif
if(v=2);'open $MSMBIN/CAPEsb.ctl';vv=SB;endif
if(v=3);'open $MSMBIN/CAPEmu.ctl';vv=MU;endif
if(v=4);'open $MSMBIN/CAPEml.ctl';vv=ML;endif
'open $MSMBIN/SRH.ctl'

'set lat 'LATLEVS
'set lon 'LONLEVS
'set xlopts 1 5 0.15'
'set ylopts 1 5 0.15'
'set map 1 1 7'
'set mpdset $mpdset'

* 'key' time for color scheme
*'set time 'DATEK

** time loop
** search tdef
l=1;while(l<=15)
'q ctlinfo'
line=sublin(result,l)
if(subwrd(line,1)=tdef)
et=subwrd(line,2)
break
endif
l=l+1;endwhile

t=1;while(t<=et)
jt=t+3
'set t 'jt

* make jst
'q dim'
line=sublin(result,5)
jst=subwrd(line,6)
year=substr(jst,9,4)
emon=substr(jst,6,3)
day =substr(jst,4,2)
hour=substr(jst,1,2)

if(emon=JAN);nmon="01";endif
if(emon=FEB);nmon="02";endif
if(emon=MAR);nmon="03";endif
if(emon=APR);nmon="04";endif
if(emon=MAY);nmon="05";endif
if(emon=JUN);nmon="06";endif
if(emon=JUL);nmon="07";endif
if(emon=AUG);nmon="08";endif
if(emon=SEP);nmon="09";endif
if(emon=OCT);nmon="10";endif
if(emon=NOV);nmon="11";endif
if(emon=DEC);nmon="12";endif

tdate=year'/'nmon'/'day' 'hour'JST (t='t')'
fdate=year''nmon''day''hour'JST'
'set t 't

'set grads off';'c';'set grads off'

d1=cape;d2=cin;d3=lfc;d4=lnb

*****************************
** drawdraw
*****************************
** cape

'set gxout shaded'
'color 0 2000 50 -kind white->green-(0)->white->orange-(0)->white->red-(0)->white->purple'
'd 'd1

*'xcbar 1 10 0.7 0.9 -fw 0.15 -fh 0.18 -edge triangle -fs 2 -fo 1 -line on'
'xcbar -fs 10'

'draw title otgrMSM 'vv' CAPE[J/kg] \ 'tdate
'printim 'IMGDIR1'/'DIRNAME'_'vv'CAPE_'fdate'.png x1100 y850 white'
'set grads off';'c';'set grads off'


** lfc
   'color 0 4000 50 -kind grainbow'
   'd 'd3
   'xcbar -fs 10'

   'draw title otgrMSM 'vv' LFC[gpm] \ 'tdate
   'printim 'IMGDIR1'/'DIRNAME'_'vv'LFC_'fdate'.png x1100 y850 white'
'set grads off';'c';'set grads off'

** lnb
   'color 0 10000 500 -kind grainbow'
   'd 'd4
   'xcbar -fs 2'
   
   'draw title otgrMSM 'vv' LNB[gpm] \ 'tdate
   'printim 'IMGDIR1'/'DIRNAME'_'vv'LNB_'fdate'.png x1100 y850 white'
'set grads off';'c';'set grads off'


** cin
'color 0 400 5 -kind white->green-(0)->white->orange-(0)->white->red-(0)->white->purple'
   'd 'd2

   'xcbar -fs 10'

   'draw title otgrMSM 'vv'CIN[J/kg] \ 'tdate
   'printim 'IMGDIR1'/'DIRNAME'_'vv'CIN_'fdate'.png x1100 y850 white'
'set grads off';'c';'set grads off'

if(v=1)
* srh3
'color 0 200 25 -kind grainbow'
'd srh3.2'
'cbarn'
   'draw title otgrMSM SRH3[m^2/s^2] \ 'tdate
   'printim 'IMGDIR1'/'DIRNAME'_SRH3_'fdate'.png x1100 y850 white'
'set grads off';'c';'set grads off'

* srh1
'color 0 200 25 -kind grainbow'
'd srh1.2'
'cbarn'
   'draw title otgrMSM SRH1[m^2/s^2] \ 'tdate
   'printim 'IMGDIR1'/'DIRNAME'_SRH1_'fdate'.png x1100 y850 white'
'set grads off';'c';'set grads off'
endif

say t a v

t=t+1;endwhile
'reinit'
v=v+1;endwhile

'quit'
exit

EOF

## RUN gs-files
cd ${GSDIR}
#files=`ls *.png | wc -l`
#if [ $files -ne 0 ]; then
#    mkdir -p org_image
#    mv *.png org_image/.
#fi

grads -blc "draw_jmbsc_msm2-x2.gs"
#grads -blc "draw_jmbsc_msm2.gs" 1>/dev/null 2>/dev/null 

cd ${CRTDIR}
exit
#*'draw title otgrMSM 'vv'CAPE[J/kg] 'vv'CIN[J/kg] ('type'T) \ 'tdate
#   *'draw title otgrMSM 'vv'LFC[gpm] 'vv'LNB[gpm] ('type'T) \ 'tdate

