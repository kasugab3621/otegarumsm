#!/bin/bash
## download JMA combined radar data from Kyoto Univ. data base
## 2012/??/?? Corded by Y.IOKA and Y.KIMURA
## 2014/12/07 Revised by Yusuke KIMURA

PARMF="../parm.sh"
. $PARMF

STEP=get_radar_echo
#-----------------------------------------------------------------
# Input these parameters
#-----------------------------------------------------------------
## initial date(UTC)
#  str_date=201308191900
## terminal date(UTC)
#  end_date=201308200800
## save path
#  SPATH="./original"
#date=$str_date
#syear=`echo $str_date | cut -c 1-4`
#smonth=`echo $str_date | cut -c 5-6`
#sday=`echo $str_date | cut -c 7-8`
#shour=`echo $str_date | cut -c 9-10`
#sminute=`echo $str_date | cut -c 11-12`
SPATH=$RDORG
end_date=$DATEED
date=$DATEST
syear=$YYS
smonth=$MMS
sday=$DDS
shour=$HHS
sminute=$NNS

smin=0

mkdir -p $SPATH
#-----------------------------------------------------------------

#-----------------------------------------------------------------
# get data
#-----------------------------------------------------------------
while [ $date -lt $end_date ]
do
    date=`$d_cmd --date "${syear}/${smonth}/${sday} ${shour}:${sminute} ${smin} minute" +%Y%m%d%H%M`
    echo $date
    year=`echo $date | cut -c 1-4`
    month=`echo $date | cut -c 5-6`
    day=`echo $date | cut -c 7-8`
    hour=`echo $date | cut -c 9-10`
    minute=`echo $date | cut -c 11-12`

    wget  http://database.rish.kyoto-u.ac.jp/arch/jmadata/data/jma-radar/synthetic/original/${year}/${month}/${day}/Z__C_RJTD_${year}${month}${day}${hour}${minute}00_RDR_JMAGPV__grib2.tar 
    tar xvf Z__C_RJTD_${year}${month}${day}${hour}${minute}00_RDR_JMAGPV__grib2.tar

    rm Z__C_RJTD_${year}${month}${day}${hour}${minute}00_RDR_JMAGPV__grib2.tar
    rm Z__C_RJTD_${year}${month}${day}${hour}${minute}00_RDR_JMAGPV_Gll2p5km_Phhlv_ANAL_grib2.bin
    mkdir -p ${SPATH}/${year}/${month}
#    mv Z__C_RJTD_${year}${month}${day}${hour}${minute}00_RDR_JMAGPV_Ggis1km_Prr10lv_ANAL_grib2.bin ${SPATH}/${year}/${month}/.
    mv Z__C_RJTD_${year}${month}${day}${hour}${minute}00_RDR_JMAGPV_Ggis1km_Prr10lv_ANAL_grib2.bin ${SPATH}/.

    smin=`expr $smin + 10`
done
#-----------------------------------------------------------------

#return 0
exit
