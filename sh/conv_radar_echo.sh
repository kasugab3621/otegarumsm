#!/bin/bash


PARMF="../parm.sh"
. $PARMF

STEP=convert_radar_echo_data

# Input these parameta
#--------------------------------------------------------------------
## directory path original data
#  opath="/mnt/data2/public_data/radar_echo"
  opath=$RDORG
## directory path to converted data
#  spath="/mnt/work2/kimura/radar_echo/data/original"
#  spath="/mnt/work2/kimura/radar_echo/data/201402_heavy_snow"
  spath=$RDDIR

## initial date
#  str_date=$1
#  str_date=201311302100
  str_date=$DATEST
## terminal date
#  end_date=$2
#  end_date=201402282350
  end_date=$DATEED
## set lat & lon
#  start_lat=34
#  end_lat=38
#  start_lon=133
#  end_lon=140
  start_lat=$LATSI
  end_lat=$LATEI
  start_lon=$LONSI
  end_lon=$LONEI
#--------------------------------------------------------------------


# Initialization
#--------------------------------------------------------------------
ln -s ${opath} ./tmp_link
mkdir -p $spath

header='Z__C_RJTD_'
footer='00_RDR_JMAGPV_Ggis1km_Prr10lv_ANAL_grib2.bin'

date=$str_date
#syear=`echo $str_date | cut -c 1-4`
#smonth=`echo $str_date | cut -c 5-6`
#sday=`echo $str_date | cut -c 7-8`
#shour=`echo $str_date | cut -c 9-10`
#sminute=`echo $str_date | cut -c 11-12`
syear=$YYS
smonth=$MMS
sday=$DDS
shour=$HHS
sminute=$NNS

smin=0
#mflag=1
#--------------------------------------------------------------------

if [ $OS = macos ]; then
    c_cmd="jmaradar2bin-mac"
else
    c_cmd="jmaradar2bin"
fi

# get data
#--------------------------------------------------------------------
while [ $date -lt $end_date ]
do
	date=`$d_cmd --date "${syear}/${smonth}/${sday} ${shour}:${sminute} ${smin} minute" +%Y%m%d%H%M`
	echo $date
	year=`echo $date | cut -c 1-4`
	month=`echo $date | cut -c 5-6`
	day=`echo $date | cut -c 7-8`
	hour=`echo $date | cut -c 9-10`
	minute=`echo $date | cut -c 11-12`

#	if [ $mflag -eq 1 -o $day -eq 01 -a $hour -eq 00 -a $minute -eq 00 ] ; then
#		echo "chenge month!"
#		sleep 1
#		mkdir -p ${spath}/${year}${month}
	#	mflag=0
#	fi

	fname=${year}${month}${day}${hour}${minute}
	echo $fname

#	ls tmp_link/$header$fname$footer
	
	#./jmaradar2bin tmp_link/${header}${fname}${footer} ${fname}.bin #${start_lon} ${end_lon} ${start_lat} ${end_lat}
	${SHDIR}/${c_cmd} tmp_link/${header}${fname}${footer} ${fname}.bin #${start_lon} ${end_lon} ${start_lat} ${end_lat}
	
#	mv ${year}${month}${day}${hour}${minute}.bin ${spath}/${year}${month}/.

#pwd
#echo $spath
#exit

	mv ${year}${month}${day}${hour}${minute}.bin ${spath}/.

    smin=`expr $smin + 10`
done
#--------------------------------------------------------------------
./g2ctl tmp_link/${header}${fname}${footer} > ${SHDIR}/view_radar.ctl

rm tmp_link
exit


#./jmaradar2bin ${opath}/$year/$month/Z__C_RJTD_${year}${month}${day}${hour}${minute}00_RDR_JMAGPV_Ggis1km_Prr10lv_ANAL_grib2.bin ${year}${month}${day}${hour}${minute}.bin 136 140.5 36.5 40
#echo ${header}${year}${month}${day}${hour}${minute}${footer}

