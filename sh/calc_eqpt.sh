#! /bin/bash

. ../parm.sh
STEP=calc-fort.sh

#memo
#undef msm=9.999e20, msm_topo=-999.9, grads(def) -9.99e8
undef=9.999e20

echo $FNUM3h

fort/rh2shp2.exe <<EOF
$FNUM3h
241,253,16
1000,975,950,925,900,850,800,700,600,500,400,300,250,200,150,100
$undef
'$MSMBIN'
'RHprs.bin','TMPprs.bin'
'SPFHprs.bin','SSPFHprs.bin'
EOF

exit

fort/eqptp.exe <<EOF
$FNUM3h
241,253,16
1000,975,950,925,900,850,800,700,600,500,400,300,250,200,150,100
$undef
'$MSMBIN'
'TMPprs.bin','SPFHprs.bin','HGTprs.bin'
'EQPTprs.bin','__ZLCLprs.bin'
EOF

exit

