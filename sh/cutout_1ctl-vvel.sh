#! /bin/bash
######################################################################
# cutout data from JRA-55 to GrADS (for any ctlfile)
# 2014.11.02 Corded by Yusuke KIMURA
# 2015.03.20 anl_p anl_surf anl_isentrop by kasuga
# 2015.05.26 Japanese -> English
# 2015.10.05 for any ctlfile by kasuga (only prs)
######################################################################

manual_dim=0 ## '1' for detail area (see below $manual_dim)

if [ $1 -z -a $2 -z  ]; then
    echo "   ****USAGE****"
    echo "   ./cutout_1ctl.sh [ctlfile] ([save-dir])"
    exit
fi

CTLF=$1
SAVEDIR=$2

ENDX=`cat $CTLF | grep xdef | awk '{print $2}'`
ENDY=`cat $CTLF | grep ydef | awk '{print $2}'`
ENDZ=`cat $CTLF | grep zdef | awk '{print $2}'`
#ITS="1"
#ITE=`cat $CTLF | grep tdef | awk '{print $2}'`
ITS="2"
ITE="2"
#VARS=`cat $CTLF | grep -A 1 vars | awk 'NR==2{print $1}'`
VARS="VVELprs"
FLNM=${SAVEDIR}/${VARS}.bin

XLEVS="1 $ENDX"
YLEVS="1 $ENDY"
ZLEVS="1 $ENDZ"

if [ $manual_dim -eq 1 ]; then 
XLEVS="1 288" 	## ALL
YLEVS="73 145" 	## 0 ~ 90N
ZLEVS="1 37"    ## 1000 hPa ~ 2 hPa  ## if anl_surf, use "1 1"
#ZLEVS="1 1"
fi

EDIA="be"	## 9. endians

if [ ! -e $1 ]; then
    echo "######### E R R O R ! ##########"
    echo "# There is no such a ctl file! #"
    echo "# make sure its name or path.  #"
    echo "################################"
    exit
fi

if [ -z $2 ]; then 
    FLNM=${VARS}.bin
else
    mkdir -p $SAVEDIR    
    FLNM=${SAVEDIR}/${VARS}.bin
fi


cat << EOF > cutouthogehoge.gs 
VARS  = "${VARS}"
FLNM  = "${FLNM}"
CTLF  = "${CTLF}"
ITS   = "${ITS}"
ITE   = "${ITE}"
XLEVS = "${XLEVS}"
YLEVS = "${YLEVS}"
ZLEVS = "${ZLEVS}"
EDIA  = "${EDIA}"
IZS = subwrd( ZLEVS, 1)
IZE = subwrd( ZLEVS, 2)

'reinit'
'open 'CTLF
'set fwrite -'EDIA' 'FLNM
'set undef dfile'
'set gxout fwrite'
'set x 'XLEVS
'set y 'YLEVS
'set z 1'
'set t 1'

IT=ITS
while(IT<=ITE)
   IZ=IZS
      say '   ==> 'VARS'(t='IT'/'ITE')'
   while(IZ<=IZE)
      'd 'VARS'(t='IT',z='IZ')'
      IZ=IZ+1
   endwhile
   IT=IT+1
endwhile

'disable fwrite'
'quit'

return

EOF

grads -blc cutouthogehoge.gs

rm -f cutouthogehoge.gs
exit
