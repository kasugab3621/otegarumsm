#!/bin/bash
## EDIT parm.sh before run this script

## otegaru jireikaiseki script
##   to download data, then draw pictures

#set -evx

cd sh
PARMF="../parm.sh"
. $PARMF
#cp $PARMF $WORKDIR/.

sdate=`$d_cmd`

## MAKE DIR
cd $WORKDIR
mkdir -p $DATADIR/jmbsc_msm
mkdir -p $DATADIR/radar_echo
mkdir -p $GSDIR
mkdir -p $CTLDIR
mkdir -p $IMGDIR
#mkdir -p image/jmbsc_msm

#mkdir -p image/radar_echo
cd ${CRTDIR}


## Radar Echo
if [ $sw_RAG -eq 1 ]; then 
   bash ${SHDIR}/get_radar_echo.sh
   cp $WORKDIR/parm.sh $DATADIR/radar_echo/parm.txt
fi

if [ $sw_RAC -eq 1 ]; then
   bash ${SHDIR}/conv_radar_echo.sh
   cp $WORKDIR/parm.sh $DATADIR/radar_echo/parm.txt
fi

if [ $sw_RAD -eq 1 ]; then 
    bash ${SHDIR}/draw_radar_echo2.sh
    cp $WORKDIR/parm.sh $IMGDIR/radar_echo/parm.txt
fi

## MSM
if [ $sw_GET -eq 1 ]; then
    bash ${SHDIR}/get_jmbsc_msm.sh
    bash ${SHDIR}/mkctl_msm.sh
    cp $WORKDIR/parm.sh $DATADIR/jmbsc_msm/parm.txt
fi

if [ $sw_BIN -eq 1 ]; then
    bash ${SHDIR}/mkctl_msm.sh
    bash ${SHDIR}/cutout_jmbsc_msm.sh
fi

if [ $sw_DRW -eq 1 ]; then
    bash ${SHDIR}/draw_jmbsc_msm2.sh
    cp $WORKDIR/parm.sh $IMGDIR/jmbsc_msm/parm.txt
fi

if [ $sw_X -eq 1 ]; then
    bash ${SHDIR}/calc_eqpt.sh
    bash ${SHDIR}/calc_cape.sh
    bash ${SHDIR}/calc_srh.sh ;# must after calc_cape.sh
fi

if [ $sw_XDRW -eq 1 ]; then
    bash ${SHDIR}/draw_jmbsc_msm2-x2.sh
    cp $WORKDIR/parm.sh $IMGDIR/jmbsc_msm/CAPE/parm.txt
fi

if [ $sw_FCST -eq 1 ]; then
    bash ${SHDIR}/get_jmbsc_msm_fcst.sh
    bash ${SHDIR}/mkctl_msm_fcst.sh
    bash ${SHDIR}/cutout_jmbsc_msm_fcst.sh
    cp $WORKDIR/parm.sh $DATADIR/jmbsc_msm/parm.txt
fi

echo "===================================="
echo "start"
echo $sdate
echo "finish"
echo `$d_cmd`

exit

