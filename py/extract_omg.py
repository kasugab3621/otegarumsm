#!/usr/bin/env python3
import os, sys
from glob import glob
import numpy as np
import pandas as pd
import xarray as xr
import pygrib


anl = 'data201801-02'
fh00s = sorted(glob(f'../{anl}/data/jmbsc_msm/org/MSM_GPV_pall_FH00-15_*.bin'))

it = len(fh00s)
iz = 16
iy, ix = 253, 241

omg_all = np.full((it, iz, iy, ix), 9.999E+20, dtype='>f4')

first = True
for i, f in enumerate(fh00s):

    if i + 1 == it:
        break

    t = f[-18:-8]  # yyyymmddhh
    print(t)

    grbs = pygrib.open(f)
    
    grbs_fcst = grbs.select(name='Vertical velocity', forecastTime=3)
    
    omg, levs = [], []
    
    for grb in grbs_fcst:
        
        levs.append(grb.level)
        omg.append(grb.values[::-1, :])  # flip north south
        if first:
            first = False
            lats, lons = grb.latlons()
            
    omg_all[i] = np.array(omg, dtype='>f4')

with open(f'../{anl}/data/jmbsc_msm/gradsbin/OMGprs.bin', 'wb') as a:
    a.write(omg_all)
