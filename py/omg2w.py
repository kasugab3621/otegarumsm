#!/usr/bin/env python3
import numpy as np
import pandas as pd


data_times = pd.date_range('2022-06-15 09',
                           '2022-07-01 06',
                           freq='3H')

lons, lats = np.arange(120, 150.1, 0.125), np.arange(22.4, 47.61, 0.1)
levs = np.array([1000, 975, 950, 925, 900, 850,
                  800, 700, 600, 500, 400, 300,
                  250, 200, 150, 100])

it = len(data_times)
iy, ix = len(lats), len(lons)
iz = len(levs)


with open('../data20220615-0630/data/jmbsc_msm/gradsbin/OMGprs.bin') as a:
    omg = np.fromfile(a, dtype='>f4').reshape(it, iz, iy, ix)

with open('../data20220615-0630/data/jmbsc_msm/gradsbin/TMPprs.bin') as a:
    t = np.fromfile(a, dtype='>f4').reshape(it, iz, iy, ix)

levss = np.empty((it, iz, iy, ix))
for i in range(len(levs)):
    levss[:, :, i, :] = levs[i]

g = 9.8
nr = 287.05

w = - nr * t * omg / levss / g / 100

with open('../data20220615-0630/data/jmbsc_msm/gradsbin/Wprs.bin', 'wb') as a:
    a.write(np.array(w, dtype='>f4'))
