! msm
program omg2w
  implicit none
  integer :: i,j,k,it
  integer,parameter :: itime=82
  integer,parameter :: ilon=241,ilat=253,ilev=16!,slon=481,slat=505
  real :: omg(ilon,ilat,ilev),w(ilon,ilat,ilev),tmp(ilon,ilat,ilev),lev(ilev)
  real,parameter :: g=9.8,nr=287.05!,nr=8.314e3
  real,parameter :: undef=9.999e20

  data lev/1000,975,950,925,900,850,800,700,600,500,400,300,250,200,150,100/

  open(1,file="../data20220615-0630/data/jmbsc_msm/gradsbin/OMGprs.bin",status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*ilev*4)
  open(2,file="../data20220615-0630/data/jmbsc_msm/gradsbin//TMPprs.bin",status="old",&
       access="direct",form="unformatted",recl=ilon*ilat*ilev*4)
  open(11,file="../data20220615-0630/data/jmbsc_msm/gradsbin/Wprs.bin",status="unknown",&
       access="direct",form="unformatted",recl=ilon*ilat*ilev*4)

  do it=1,itime
  
     read(1,rec=it) omg
     read(2,rec=it) tmp
     print *, "now drawing ",it,"/",itime
     
     do k=1,ilev
        do j=1,ilat
           do i=1,ilon
              
              w(i,j,k)=-nr*tmp(i,j,k)*omg(i,j,k)/lev(k)/g/100
              !              print *, w(i,j,k)
              !              print *, w(i,j,k)
              !              print *, w(i,j,k)
!              stop
           end do
        end do
     end do
     
     write(11,rec=it) (((w(i,j,k),i=1,ilon),j=1,ilat),k=1,ilev)

  end do
  stop
end program omg2w

     
              
