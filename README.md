# お軽事例解析用ダウンロード・作図スクリプト
[京都大学生存圏](http://database.rish.kyoto-u.ac.jp/arch/jmadata/)のサーバーより，指定した期間の[気象庁メソ数値予報モデルGPV(MSM)](http://www.jmbsc.or.jp/jp/online/file/f-online10200.html)と[気象庁全国合成レーダーGPV](http://www.jmbsc.or.jp/jp/online/file/f-online30100.html)をダウンロードし，作図を行うスクリプト群です．
基本的にLinux(特にUbuntu)での利用を前提にしています．MacOSでも一応動作しますが，下の説明を参照してください．

使い方は，parm.shを編集して，otegaru-msm.shを実行するだけです．

実行は，`./otegaru-msm.sh `

<br>


## 注意事項
* このスクリプトは「一時的に」MSMや全国合成レーダーを「確認」するために作成された装置です．
* くれぐれもこの装置で作成した図をそのままゼミで利用しないこと．しばしば先生に怒られます．
* 「伝えたいことをわかりやすく伝える」為には（微）修正が必要不可欠です．

<br>

## ダウンロード＆インストール
初回のみ各種ツール群をインストールする必要があります．
[INSTALL.md](/INSTALL.md)を参照してください．

<br>

# parm.sh に設定を書き込む
適当なテキストエディタを用いて，各変数に適当な値を代入してください．  

* ANLNAME	: 作成される解析用ディレクトリの名前 *1
* DATEST1	: 解析期間の開始日時(JST) *2
* DATEED1	: 解析期間の終了日時(JST)
* DATEKEY	: 現象発現の最近某時刻(JST) *3
* LATSI		: 対象エリアの南端緯度 *4
* LATEI		: 対象エリアの北端緯度
* LONSI		: 対象エリアの西端経度
* LONEI		: 対象エリアの東端経度
* LEVELS	: 作図する気圧面の指定 *5

(*1) parm.shと同じ階層(otegarumsm/)に解析用ディレクトリ(ANLNAME)を作成し，その中へデータや画像が保存されます．
ここを書き換えずに異なる解析をしてしまった場合，前回の結果の一部が失われてしまいますので，注意してください．  
(*2) 時刻はJSTで指定し，自動でUTCに変換し該当データをダウンロード後，作図時はJSTへ再度変換している．データ名はctlファイルも含めUTCのままなので注意．  
(*3) DATEKEYの時刻に合わせて作図時の色合いやコンターの間隔を自動で設定します．  
(*4) このエリア指定は作図時のみ適応されます．データそのものの領域や容量は変更しません．  
(*5) [気象業務支援センター](http://www.jmbsc.or.jp/jp/online/file/f-online10200.html)の説明を参考にしてください．なお地表面データは必ず作図されます．  

解析方法に関して複数のスイッチがあり，それぞれ

* sw_RAG --> 全国合成レーダーの取得
* sw_RAD --> 全国合成レーダーの描画
* sw_GET --> MSMの取得(grib2形式)
* sw_BIN --> MSMをgrads形式へデータ変換
* sw_DRW --> MSMの描画
* sw_X   --> SPFH,EQPT,CAPE,SREHの計算
* sw_XDRW--> CAPE, SREHの描画
* sw_FCST--> 予報値データの取得＆整理 (VVELはsh/cutout_1ctl-vvel.shを利用)

という対応をしています．1にすればON（実行される），0にすればOFF（実行されない）です．
当然ながら，先にデータを取得しないと作図は失敗します．

MSMは3時間間隔なのに対し合成レーダーは10分間隔データなので，データの取得に膨大な時間がかかることがあります．
状況に応じてそれぞれにふさわしい解析時刻を設定し，データごとに分けて実行することを推奨します．

＊MacOSで実行する場合はOS=macosとしてください．GNU coreutilsを必要とします．

<br>

# otegarum-sm.shを実行する
基本的にotegaru-msm.shの内部は編集する必要はありません．
以下のコマンドを端末に記入してEnterしてください．
```
$ ./otegaru-msm.sh
```
通信速度やPCの処理能力によりますが，1日分の解析にはだいたい5~10分の時間を要します（環境によります）．  
長時間のデータ取得はホスト側から通信規制をかけられることがあるので，その場合は期間を小分けにして時間を開けて実行してください(ヒント，sleepコマンド)．


## 出力ファイルの確認
スクリプトが正常に動作すると`$ANLNAME`ディレクトリが生成し，内部には以下にディレクトリが生成されます．

* ctl/: 配布元そのままのフォーマットのデータ
* data/: フラットバイナリに変換されたデータ
* gs/: 作図に利用するGrADSスクリプト
* image/: 自動生成された図

内部のjmbsc_msmとradar_echoはそれぞれ，MSMと合成レーダーを示します．

MSMの変換データは`data/jmbsc_msm/grads/gradsbin`へ格納され，ファイル名と鉛直方向の次元で区別しています．

* ???prs.bin  : 等気圧面(1000~100hPa, 16層)
* ???z.bin    : 等高度面(0~10000m, 21層)
* ???sfc.bin  : 地表面，その他2次元データ
* ???2m.bin   : 地表2m
* ???10m.bin  : 地表10m


MSMの図は`image/jmbsc_msm`に格納され，

* VOR/ : 相対渦度と風
* TMP/ : 気温とジオポテンシャル高度と風
* RH/ : 相対湿度と風
* DIV/ : 収束発散と風
* CLA/ : 全雲量
* CAPE/ : CAPE, CIN, LNB, LFCとSREH

が生成されます．

otegaru-msm.shの実行によりデータをダウンロードした場合，
parm.shのコピーを`data/jmbsc_msm`もしくは`data/radar_echo`にparm.txtとして生成します．  
同様に，otegaru-msm.shの実行により図を作図した場合もparm.shのコピーを`image/jmbsc_msm`もしくは`image/radar_echo`にparm.txtして生成します．

parm.shを再編集したり$ANLNAME/gs/draw_jmbsc_msm2.gsを編集することで自動生成する図を変更できます．ただし，再度otegaru-msm.shを実行するとimageの中身は上書き保存され前回のものが消えてしまうため，
```
mv image image2
```
などして避難させておくと安全です．また，
```
$ grads -blc draw_jmbsc_msm2.gs
```
を実行することで作図のみ実行できます．
（draw_jmbsc_msm2.gsは非常に複雑なため，自身の目的に合わせて(例えばdataディレクトリの中で)GrADSスクリプトを作成することをお勧めします）

<br>

## 高画質出力について

EPS出力を介した高画像出力 (時間かかる＆大容量(約1MB/1枚))を実装しています．   
スイッチと同様に，`EPS=1`でオン，`EPS=0`でオフです．   
ImageMagickのconvertコマンドを利用しますが，設定ファイル(`/etc/ImageMagick-6/policy.xml`)をあらかじめ編集しておく必要がある場合があります．[convertに関して](http://zairyo.susi.oita-u.ac.jp/wordpress/?p=7385)

```
<policy domain="coder" rights="none" pattern="PS" />
<policy domain="coder" rights="none" pattern="EPS" />
```
↓
```
<policy domain="coder" rights="read|write" pattern="PS" />
<policy domain="coder" rights="read|write" pattern="EPS" />
```

<br>

## CAPEの計算について

CAPEで利用する持ち上げ空気塊の気温プロファイルは吉崎・加藤 (2007, 豪雨豪雪の気象学, 付録A-3)に基づいて計算しております．また，持ち上げる空気塊の種類に応じて名前を変えています．

* CAPE5.bin  : 地上500m
* CAPEsb.bin  : 地表面(SBCAPE)
* CAPEmu.bin  : 地上3000m以下で最も不安定となる高度(MUCAPE)
* CAPEml.bin  : 地上1000m以下の気温を平均(MLCAPE)，バグ未修正(meanleyer.f90)につき現在出力していません

SREHで利用するストームの移動速度はBunkers et al. (2000)に基づいて計算しております．

CAPEやSREHを計算すると中間的なデータを生成します(__で始まるデータ)．PCの容量を圧迫するようでしたら削除してください．


<br>

## VVELを利用する (中級者向け)
2017年2月28日よりMSMにasucaが導入され，計算に鉛直流の解析値を用いない方式となりました(https://www.jma.go.jp/jma/kishou/books/nwptext/50/chapter2.pdf)．  
ゆえに，予報値よりVVELを取得する必要がありますが，現在そのための自動システムはotegaru-msm内では実装しておりません．（誰か作って）

ここでは１タイムステップずつバイナリ化する手法を紹介します．  
まずsh/cutout_1ctl-vvel.sh を $ANLDIR/ctl へコピーします，
```
$ sh cutout_1ctl-vvel.sh view_MSM_pall_yyyymmddhh.ctl
```
すると，grads上で`set t 2`(一番初めの予報値にset)したVVELprs.binが生成されるので逐一名前を変更し，
```
$ mv VVELprs.bin VVELprs_yyyymmddhh.bin
```
最終的にcatすれば解析値によるデータと似たデータを作成できます．（タイムステップは１つずれる）
```
$ cat VVELprs_*.bin > VVELprs.bin
```

<br>

## ギャラリー

<img src=".img/jmbsc_msm_pall_TZUV500_2021050200JST.png" width="500px">

<img src=".img/RadarEcho_int-Pricip.png" width="500px">

