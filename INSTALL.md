# ダウンロード
お好きなディレクトリで以下のコマンドを実行して下さい
```
$ git clone https://gitlab.com/naos1/otegarumsm.git
```
するとotegarumsmディレクトリが生成します．

<br>

# 準備の流れ
1. Fortranのコンパイル
1. GrADS (gribmap, color.gs)のインストール
1. 実行権限の付与とパス(g2ctl, jmaradar2bin)


なお，atmos4ではGrADS, g2ctl, jmaradaar2binはインストール済みですので，FortranコンパイルだけでOK

<br>

# 各種ツールの準備

# Fortranのコンパイル
sh/fortへ移動し，compile.shを実行してください．
```
$ cd sh/fort
$ sh compile.sh
```
コンパイルや実行時にエラーが出る場合，compile.sh内部でFortranコンパイラーを指定する箇所がありますので確認してください．
FortranはCAPEやSREHやCAPEに関するパラメタの計算にのみ利用します．最悪コンパイル成功しなくても大丈夫です．  
Endianはbig_endianで統一していますので注意してください．

<br>

## GrADS (gribmap, color.gs)のインストール
メインの描画ソフト．
インストール方法はWebやlinux_firststep.txtを参照してください．

gribmapも必要です．gradsをインストール済みであれば`/usr/local/grads-2.0.2/bin/grads`にいるはずなので，確認してください．

color.gs ([Kodamaさんのサイト](http://kodama.fubuki.info/wiki/wiki.cgi/GrADS/script/color.gs?lang=jp)) も必要です．
GrADSを起動し，colorと実行してオプションの説明が出てくればインストール済み．
`Unknown command: color`と出た場合インストールが必要ですので，上記のリンクを参照してくだい．

<br>

## 実行権限の付与
### g2ctl
GRIB2よりctlファイルを生成するツール．
```
$ chmod +x g2ctl
```
実行して説明文が出れば成功
```
$ ./g2ctl
```

### jmaradar2bin
清水慎吾(防災科研)さんが以前ネットで公開していた，気象庁全国合成レーダをバイナリに変換するツール．
マックの場合，下記の`jmaradar2bin`を`jmaradar2bin-mac`に置き換えてください．
```
$ chmod +x jmaradar2bin
```
こちらも実行して説明文（`ARGV ERROR: infile outfile [slon] [elon] [slat] [elat]`）を確認してください
```
$ ./jmaradar2bin
```
もし他のエラーが出てしまう場合，makeします（中級者向け）．sh/read_jmaradarへ移動し，Makefileを編集し適当なCコンパイラーを変数CCに指定し（MacOSであるNAOS-47の環境，上部参照，ではなぜかgccは通らずiccでコンパイル成功しました），makeを実行してください．

### パスを通す
g2ctl,jmaradar2binをパスの通ったディレクトリへコピーしてください，例えば，
```
$ sudo cp g2ctl jmaradar2bin /usr/local/bin/.
```
でいけると思います．
<br>

