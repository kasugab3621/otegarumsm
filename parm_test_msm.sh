#!/bin/bash
##  SEE README.txt first

## name of analysis dir
ANLNAME="test_msm"

## time duration (JST) 
DATEST1="2018/01/01 00:00"; #jst
DATEED1="2018/01/01 03:00"; #jst
## for MSM, calibrate color scheme to 'key time' (JST) of the event
DATEKEY="2018/01/01 00:00"

## area of analysis (default 22.4/47.6/120/150)
LATSI=22.4;LATEI=47.6;LONSI=120;LONEI=150
#LATSI=27;LATEI=44;LONSI=120;LONEI=145

## levels for drowing
# choose from 1000 975 925 900 850 800 700 600 500 400 300 250* 200 150 100
# *: no RH above 250hPa
LEVELS="925 850 700 600 500 200"

## switch (1:on, 0:off)
sw_RAG=0 	 ## get radar
sw_RAC=0     ## convert radar
sw_RAD=0	 ## draw radar
sw_GET=1         ## get msm
sw_BIN=1         ## convert msm (grib2 to grads binary)
sw_DRW=1         ## draw msm
sw_X=1           ## calc. cape & srh
sw_XDRW=1        ## draw cape
sw_FCST=0        ## save forecast data

## use EPS (high resolution figure)
EPS=0

## select linux or macos
OS="linux"

## alias for grads (test)
shopt -s expand_aliases
alias grads="grads -l -h GD"

#########################
## DO NOT CHANGE BELOW ##
#########################

## mpdset when draw <lowres|mres|hires|hires_pref|nmap|>
mpdset=hires_pref

## main
STEP=set_parm

## organize dirs
CRTDIR=`pwd`
cd ../ ; WORKDIR=`pwd` ;cd $CRTDIR
SHDIR=$WORKDIR/sh
DATADIR=$WORKDIR/$ANLNAME/data
GSDIR=$WORKDIR/$ANLNAME/gs
CTLDIR=$WORKDIR/$ANLNAME/ctl
IMGDIR=$WORKDIR/$ANLNAME/image


## rader 
RDORG=$DATADIR/radar_echo/org  ## original data
RDDIR=$DATADIR/radar_echo      ## converted data

## MSM
MSMORG=$DATADIR/jmbsc_msm/org   ## original data
MSMBIN=$DATADIR/jmbsc_msm/gradsbin   ## conbined data
MSMDIR=$DATADIR/jmbsc_msm       ## converted data   
TOPO=$SHDIR/topo/rtopo

case $OS in
    macos)
	d_cmd="gdate"
	;;
    linux)
	d_cmd="date"
	;;
    *)
	echo 'set OS="linux" or OS="macos"'
	exit 1
esac

## SET_DATE *convert JST -> UTC
DATESTI=`$d_cmd --date "$DATEST1 9 hour ago" +"%Y/%m/%d %H:%M"`
DATEEDI=`$d_cmd --date "$DATEED1 9 hour ago" +"%Y/%m/%d %H:%M"`
DATEKEI=`$d_cmd --date "$DATEKEY 9 hour ago" +"%Y/%m/%d %H:%M"`
DATEST=`$d_cmd --date "$DATESTI" +%Y%m%d%H%M`
    SS=`$d_cmd --date "$DATESTI" +%s`
YYS=`echo $DATEST | cut -c 1-4`
MMS=`echo $DATEST | cut -c 5-6`
DDS=`echo $DATEST | cut -c 7-8`
HHS=`echo $DATEST | cut -c 9-10`
NNS=`echo $DATEST | cut -c 11-12`
DATEED=`$d_cmd --date "$DATEEDI" +%Y%m%d%H%M`
    ES=`$d_cmd --date "$DATEEDI" +%s`
YYE=`echo $DATEED | cut -c 1-4`
MME=`echo $DATEED | cut -c 5-6`
DDE=`echo $DATEED | cut -c 7-8`
HHE=`echo $DATEED | cut -c 9-10`
NNE=`echo $DATEED | cut -c 11-12`
DATEK=`LC_ALL=C $d_cmd --date "$DATEKEI" +%H"Z"%d%b%Y`

diff=`expr $ES - $SS`
min=`expr $diff / 600`
FNUM10m=`expr $min + 1`
hour=`expr $diff / 3600 / 3`
FNUM3h=`expr $hour + 1`

